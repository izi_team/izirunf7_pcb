EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 10
Title "izirunf7"
Date "2020-08-31"
Rev "A"
Comp "IZITRON"
Comment1 "www.izitron.com"
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Text Notes 6235 760  2    118  Italic 24
SDRAM
Text Label 3870 5040 0    50   ~ 0
DQ0
Text Label 3870 4840 0    50   ~ 0
DQ2
Text Label 3870 4940 0    50   ~ 0
DQ1
Text Label 2170 2540 0    50   ~ 0
A6
Text Label 2170 2840 0    50   ~ 0
A3
Text Label 2170 3440 0    50   ~ 0
BA0
Text Label 2170 2740 0    50   ~ 0
A4
Text Label 2170 2640 0    50   ~ 0
A5
Text Label 2170 2440 0    50   ~ 0
A7
Text Label 2170 2040 0    50   ~ 0
A11
Text Label 2170 1940 0    50   ~ 0
A12
Text Label 2170 2140 0    50   ~ 0
A10
Text Label 2170 2340 0    50   ~ 0
A8
Text Label 2170 2940 0    50   ~ 0
A2
Text Label 2170 2240 0    50   ~ 0
A9
Text Label 2170 3140 0    50   ~ 0
A0
Text Label 2170 3040 0    50   ~ 0
A1
Text Label 2170 3640 0    50   ~ 0
WE
Text Label 2170 3340 0    50   ~ 0
BA1
Text Label 2170 3940 0    50   ~ 0
CS0
Text Label 2170 3740 0    50   ~ 0
CAS
Text Label 2170 3840 0    50   ~ 0
RAS
Text Label 2170 4140 0    50   ~ 0
CLK
Text Label 2170 4040 0    50   ~ 0
CKE0
Entry Wire Line
	1970 3540 2070 3640
Entry Wire Line
	1970 3940 2070 4040
Entry Wire Line
	1970 3240 2070 3340
Entry Wire Line
	1970 3040 2070 3140
Entry Wire Line
	1970 2440 2070 2540
Entry Wire Line
	1970 2740 2070 2840
Entry Wire Line
	1970 3740 2070 3840
Entry Wire Line
	1970 2140 2070 2240
Entry Wire Line
	1970 2340 2070 2440
Entry Wire Line
	1970 2640 2070 2740
Entry Wire Line
	1970 2840 2070 2940
Entry Wire Line
	1970 1940 2070 2040
Entry Wire Line
	1970 2040 2070 2140
Entry Wire Line
	1970 3640 2070 3740
Entry Wire Line
	1970 3840 2070 3940
Entry Wire Line
	1970 2540 2070 2640
Entry Wire Line
	1970 1840 2070 1940
Entry Wire Line
	1970 2240 2070 2340
Entry Wire Line
	1970 3340 2070 3440
Entry Wire Line
	1970 2940 2070 3040
Text Label 2170 4640 0    50   ~ 0
DQM0
Text Label 2170 4540 0    50   ~ 0
DQM1
Text Label 2170 4340 0    50   ~ 0
DQM3
Text Label 2170 4440 0    50   ~ 0
DQM2
Entry Wire Line
	1970 4240 2070 4340
Entry Wire Line
	1970 4340 2070 4440
Entry Wire Line
	1970 4040 2070 4140
Entry Wire Line
	1970 4440 2070 4540
Entry Wire Line
	1970 4540 2070 4640
Text Label 3870 2440 0    50   ~ 0
DQ26
Text Label 3870 2140 0    50   ~ 0
DQ29
Text Label 3870 1940 0    50   ~ 0
DQ31
Text Label 3870 2040 0    50   ~ 0
DQ30
Text Label 3870 2240 0    50   ~ 0
DQ28
Text Label 3870 2340 0    50   ~ 0
DQ27
Text Label 3870 2640 0    50   ~ 0
DQ24
Text Label 3870 2540 0    50   ~ 0
DQ25
Text Label 3870 4740 0    50   ~ 0
DQ3
Text Label 3870 4140 0    50   ~ 0
DQ9
Text Label 3870 4540 0    50   ~ 0
DQ5
Text Label 3870 4440 0    50   ~ 0
DQ6
Text Label 3870 3440 0    50   ~ 0
DQ16
Text Label 3870 3540 0    50   ~ 0
DQ15
Text Label 3870 4040 0    50   ~ 0
DQ10
Text Label 3870 3740 0    50   ~ 0
DQ13
Text Label 3870 4640 0    50   ~ 0
DQ4
Text Label 3870 3940 0    50   ~ 0
DQ11
Text Label 3870 4340 0    50   ~ 0
DQ7
Text Label 3870 3840 0    50   ~ 0
DQ12
Text Label 3870 2740 0    50   ~ 0
DQ23
Text Label 3870 2840 0    50   ~ 0
DQ22
Text Label 3870 2940 0    50   ~ 0
DQ21
Text Label 3870 3040 0    50   ~ 0
DQ20
Text Label 3870 3140 0    50   ~ 0
DQ19
Text Label 3870 3240 0    50   ~ 0
DQ18
Text Label 3870 3340 0    50   ~ 0
DQ17
Text Label 3870 3640 0    50   ~ 0
DQ14
Text Label 3870 4240 0    50   ~ 0
DQ8
Wire Wire Line
	1620 5940 1620 6040
Wire Wire Line
	1620 5640 1620 5740
Wire Wire Line
	1870 5740 1620 5740
Wire Wire Line
	1620 5540 1620 5640
Wire Wire Line
	1620 5440 1620 5540
Wire Wire Line
	1620 5840 1620 5940
Wire Wire Line
	1620 5740 1620 5840
Wire Wire Line
	1870 5840 1620 5840
Wire Wire Line
	1870 5940 1620 5940
Wire Wire Line
	1870 5440 1620 5440
Wire Wire Line
	1870 6040 1620 6040
Wire Wire Line
	1870 5640 1620 5640
Wire Wire Line
	1870 6140 1620 6140
Wire Wire Line
	1870 5540 1620 5540
Wire Wire Line
	1620 6040 1620 6140
Wire Wire Line
	1620 6140 1620 6240
Wire Wire Line
	1620 6240 1620 6340
Connection ~ 1620 6640
Connection ~ 1620 6140
Connection ~ 1620 6340
Connection ~ 2370 5440
Connection ~ 1620 5540
Connection ~ 2370 5940
Connection ~ 1620 6240
Connection ~ 2370 6340
Connection ~ 1620 6040
Connection ~ 2370 5840
Connection ~ 2370 5640
Connection ~ 1620 5840
Connection ~ 1620 6540
Connection ~ 2370 5540
Connection ~ 2370 6440
Connection ~ 1620 5640
Connection ~ 2370 5740
Connection ~ 1620 5740
Connection ~ 2370 6140
Connection ~ 2370 6240
Connection ~ 2370 6040
Connection ~ 2370 5340
Connection ~ 1620 5440
Connection ~ 1620 6440
Connection ~ 1620 5940
Wire Wire Line
	2070 5440 2370 5440
Wire Wire Line
	2070 5540 2370 5540
Wire Wire Line
	1870 6240 1620 6240
Wire Wire Line
	1620 6440 1620 6540
Wire Wire Line
	2370 6340 2370 6440
Wire Wire Line
	2370 5640 2370 5740
Wire Wire Line
	2070 6440 2370 6440
Wire Wire Line
	1870 6340 1620 6340
Wire Wire Line
	2370 5340 2370 5440
Wire Wire Line
	2070 6540 2370 6540
Wire Wire Line
	1870 6440 1620 6440
Wire Wire Line
	1870 6540 1620 6540
Wire Wire Line
	1620 6640 1620 6790
Wire Wire Line
	1870 6640 1620 6640
Wire Wire Line
	2370 5740 2370 5840
Wire Wire Line
	2370 5840 2370 5940
Wire Wire Line
	2370 5190 2370 5340
Wire Wire Line
	2370 5540 2370 5640
Wire Wire Line
	2070 6340 2370 6340
Wire Wire Line
	2070 6140 2370 6140
Wire Wire Line
	1870 5340 1620 5340
Wire Wire Line
	2370 6040 2370 6140
Wire Wire Line
	2070 5640 2370 5640
Wire Wire Line
	2370 6540 2370 6640
Wire Wire Line
	2370 6140 2370 6240
Wire Wire Line
	2370 6240 2370 6340
Wire Wire Line
	2070 6640 2370 6640
Wire Wire Line
	2070 6040 2370 6040
Wire Wire Line
	2370 5940 2370 6040
Wire Wire Line
	2070 5940 2370 5940
Wire Wire Line
	1620 6540 1620 6640
Wire Wire Line
	1620 5340 1620 5440
Wire Wire Line
	2070 5340 2370 5340
Wire Wire Line
	2370 5440 2370 5540
Wire Wire Line
	2070 5740 2370 5740
Wire Wire Line
	2070 5840 2370 5840
Wire Wire Line
	2070 6240 2370 6240
Wire Wire Line
	2370 6440 2370 6540
Wire Wire Line
	1620 6340 1620 6440
Wire Wire Line
	4220 5540 4220 5640
Wire Wire Line
	4220 5440 4220 5540
Wire Wire Line
	4220 5840 4220 5940
Wire Wire Line
	4220 6540 4220 6640
Wire Wire Line
	4220 6640 4220 6790
Wire Wire Line
	4220 5940 4220 6040
Wire Wire Line
	4220 6340 4220 6440
Wire Wire Line
	4220 5340 4220 5440
Wire Wire Line
	4220 6440 4220 6540
Wire Wire Line
	4220 6240 4220 6340
Wire Wire Line
	4220 6140 4220 6240
Wire Wire Line
	4220 5740 4220 5840
Wire Wire Line
	4220 5640 4220 5740
Wire Wire Line
	4220 6040 4220 6140
Connection ~ 4220 6440
Connection ~ 4220 5640
Connection ~ 4220 6240
Connection ~ 4220 5840
Connection ~ 4220 6540
Connection ~ 4220 6340
Connection ~ 4220 5440
Connection ~ 4220 6640
Connection ~ 4220 6140
Connection ~ 4220 6040
Connection ~ 4220 5740
Connection ~ 4220 5540
Connection ~ 4220 5940
Entry Wire Line
	4170 2140 4270 2040
Entry Wire Line
	4170 3440 4270 3340
Entry Wire Line
	4170 4140 4270 4040
Entry Wire Line
	4170 2840 4270 2740
Entry Wire Line
	4170 2440 4270 2340
Entry Wire Line
	4170 3140 4270 3040
Entry Wire Line
	4170 2240 4270 2140
Entry Wire Line
	4170 2940 4270 2840
Entry Wire Line
	4170 3340 4270 3240
Entry Wire Line
	4170 3540 4270 3440
Entry Wire Line
	4170 3940 4270 3840
Entry Wire Line
	4170 3840 4270 3740
Entry Wire Line
	4170 4440 4270 4340
Entry Wire Line
	4170 4740 4270 4640
Entry Wire Line
	4170 3640 4270 3540
Entry Wire Line
	4170 4540 4270 4440
Entry Wire Line
	4170 2740 4270 2640
Entry Wire Line
	4170 4040 4270 3940
Entry Wire Line
	4170 3040 4270 2940
Entry Wire Line
	4170 1940 4270 1840
Entry Wire Line
	4170 4940 4270 4840
Entry Wire Line
	4170 2040 4270 1940
Entry Wire Line
	4170 4640 4270 4540
Entry Wire Line
	4170 3740 4270 3640
Entry Wire Line
	4170 5040 4270 4940
Entry Wire Line
	4170 4340 4270 4240
Entry Wire Line
	4170 2340 4270 2240
Entry Wire Line
	4170 4240 4270 4140
Entry Wire Line
	4170 3240 4270 3140
Entry Wire Line
	4170 4840 4270 4740
Entry Wire Line
	4170 2540 4270 2440
Entry Wire Line
	4170 2640 4270 2540
Connection ~ 2370 6540
Connection ~ 2370 6640
$Comp
L power:GND #PWR03
U 1 1 5C06E91C
P 1620 6790
F 0 "#PWR03" H 1620 6540 59  0001 L CIB
F 1 "GND" H 1625 6617 39  0000 L CIB
F 2 "" H 1620 6790 39  0001 L TIB
F 3 "" H 1620 6790 39  0001 L TIB
	1    1620 6790
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C25
U 1 1 5C03EC61
P 1970 6540
F 0 "C25" V 1930 6335 59  0000 L CIB
F 1 "100nF" V 1935 6605 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 6540 39  0001 L TIB
F 3 "~" H 1970 6540 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 6540
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C1
U 1 1 5C03E92B
P 1970 5340
F 0 "C1" V 1920 5185 59  0000 L CIB
F 1 "100nF" V 1930 5395 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 5340 39  0001 L TIB
F 3 "~" H 1970 5340 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 5340
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5C03CBCD
P 4220 6790
F 0 "#PWR04" H 4220 6540 59  0001 L CIB
F 1 "GND" H 4225 6617 39  0000 L CIB
F 2 "" H 4220 6790 39  0001 L TIB
F 3 "" H 4220 6790 39  0001 L TIB
	1    4220 6790
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C11
U 1 1 5C03EAE7
P 1970 5840
F 0 "C11" V 1930 5630 59  0000 L CIB
F 1 "100nF" V 1935 5895 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 5840 39  0001 L TIB
F 3 "~" H 1970 5840 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 5840
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C23
U 1 1 5C03EC2F
P 1970 6440
F 0 "C23" V 1925 6230 59  0000 L CIB
F 1 "100nF" V 1935 6510 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 6440 39  0001 L TIB
F 3 "~" H 1970 6440 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 6440
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C13
U 1 1 5C03EB09
P 1970 5940
F 0 "C13" V 1930 5725 59  0000 L CIB
F 1 "100nF" V 1935 5995 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 5940 39  0001 L TIB
F 3 "~" H 1970 5940 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 5940
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR01
U 1 1 5C03F684
P 2370 5190
F 0 "#PWR01" H 2370 5040 59  0001 L CIB
F 1 "+3V3" H 2295 5340 39  0000 L CIB
F 2 "" H 2370 5190 39  0001 L TIB
F 3 "" H 2370 5190 39  0001 L TIB
	1    2370 5190
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C27
U 1 1 5C03EC91
P 1970 6640
F 0 "C27" V 1925 6435 59  0000 L CIB
F 1 "100nF" V 1935 6705 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 6640 39  0001 L TIB
F 3 "~" H 1970 6640 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 6640
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C7
U 1 1 5C03EAA9
P 1970 5640
F 0 "C7" V 1930 5480 59  0000 L CIB
F 1 "100nF" V 1930 5695 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 5640 39  0001 L TIB
F 3 "~" H 1970 5640 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 5640
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C17
U 1 1 5C03EB53
P 1970 6140
F 0 "C17" V 1925 5930 59  0000 L CIB
F 1 "100nF" V 1935 6200 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 6140 39  0001 L TIB
F 3 "~" H 1970 6140 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 6140
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C21
U 1 1 5C03EBBD
P 1970 6340
F 0 "C21" V 1925 6135 59  0000 L CIB
F 1 "100nF" V 1935 6405 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 6340 39  0001 L TIB
F 3 "~" H 1970 6340 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 6340
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C19
U 1 1 5C03EB93
P 1970 6240
F 0 "C19" V 1925 6035 59  0000 L CIB
F 1 "100nF" V 1935 6305 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 6240 39  0001 L TIB
F 3 "~" H 1970 6240 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 6240
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C3
U 1 1 5C03E9CA
P 1970 5440
F 0 "C3" V 1925 5275 59  0000 L CIB
F 1 "100nF" V 1935 5490 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 5440 39  0001 L TIB
F 3 "~" H 1970 5440 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 5440
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C15
U 1 1 5C03EB2D
P 1970 6040
F 0 "C15" V 1925 5830 59  0000 L CIB
F 1 "100nF" V 1935 6100 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 6040 39  0001 L TIB
F 3 "~" H 1970 6040 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 6040
	0    1    1    0   
$EndComp
Wire Wire Line
	2070 2140 2620 2140
Wire Wire Line
	2070 2540 2620 2540
Wire Wire Line
	2070 2340 2620 2340
Wire Wire Line
	2070 3140 2620 3140
Wire Wire Line
	2620 2640 2070 2640
Wire Wire Line
	2620 2240 2070 2240
Wire Wire Line
	2620 2840 2070 2840
Wire Wire Line
	2620 3040 2070 3040
Wire Wire Line
	2620 3340 2070 3340
Wire Wire Line
	2620 2040 2070 2040
Wire Wire Line
	2070 2740 2620 2740
Wire Wire Line
	2070 3440 2620 3440
Wire Wire Line
	2070 1940 2620 1940
Wire Wire Line
	2620 2440 2070 2440
Wire Wire Line
	2070 2940 2620 2940
Wire Wire Line
	3820 2640 4170 2640
Wire Wire Line
	3820 2440 4170 2440
Wire Wire Line
	4170 2140 3820 2140
Wire Wire Line
	4170 1940 3820 1940
Wire Wire Line
	4170 2540 3820 2540
Wire Wire Line
	4170 2740 3820 2740
Wire Wire Line
	3820 2040 4170 2040
Wire Wire Line
	4170 2340 3820 2340
Wire Wire Line
	3820 2240 4170 2240
Wire Wire Line
	2070 4040 2620 4040
Wire Wire Line
	2070 3940 2620 3940
Wire Wire Line
	2070 3840 2620 3840
Wire Wire Line
	2070 3640 2620 3640
Wire Wire Line
	2070 4440 2620 4440
Wire Wire Line
	2620 4540 2070 4540
Wire Wire Line
	2620 4340 2070 4340
Wire Wire Line
	2070 4140 2620 4140
Wire Wire Line
	2070 3740 2620 3740
Wire Wire Line
	2070 4640 2620 4640
Wire Wire Line
	3820 4340 4170 4340
Wire Wire Line
	3820 4240 4170 4240
Wire Wire Line
	3820 2840 4170 2840
Wire Wire Line
	3820 2940 4170 2940
Wire Wire Line
	3820 4140 4170 4140
Wire Wire Line
	3820 4440 4170 4440
Wire Wire Line
	3820 3540 4170 3540
Wire Wire Line
	3820 4840 4170 4840
Wire Wire Line
	3820 4040 4170 4040
Wire Wire Line
	3820 3040 4170 3040
Wire Wire Line
	3820 4640 4170 4640
Wire Wire Line
	3820 4540 4170 4540
Wire Wire Line
	3820 3140 4170 3140
Wire Wire Line
	3820 4740 4170 4740
Wire Wire Line
	3820 3240 4170 3240
Wire Wire Line
	3820 3340 4170 3340
Wire Wire Line
	3820 3440 4170 3440
Wire Wire Line
	3820 3740 4170 3740
Wire Wire Line
	3820 3640 4170 3640
Wire Wire Line
	3820 5040 4170 5040
Wire Wire Line
	3820 4940 4170 4940
Wire Wire Line
	3820 3840 4170 3840
Wire Wire Line
	3820 3940 4170 3940
Wire Wire Line
	2370 6540 2620 6540
Wire Wire Line
	2370 6240 2620 6240
Wire Wire Line
	2370 6640 2620 6640
Wire Wire Line
	2370 6340 2620 6340
Wire Wire Line
	2370 6140 2620 6140
Wire Wire Line
	2370 5940 2620 5940
Wire Wire Line
	2370 5540 2620 5540
Wire Wire Line
	2370 5440 2620 5440
Wire Wire Line
	2370 5640 2620 5640
Wire Wire Line
	2370 6040 2620 6040
Wire Wire Line
	2370 5740 2620 5740
Wire Wire Line
	2370 6440 2620 6440
Wire Wire Line
	2370 5840 2620 5840
Wire Wire Line
	2370 5340 2620 5340
Wire Wire Line
	3820 6240 4220 6240
Wire Wire Line
	3820 6540 4220 6540
Wire Wire Line
	3820 6340 4220 6340
Wire Wire Line
	3820 6140 4220 6140
Wire Wire Line
	3820 6440 4220 6440
Wire Wire Line
	3820 5340 4220 5340
Wire Wire Line
	3820 5740 4220 5740
Wire Wire Line
	3820 6640 4220 6640
Wire Wire Line
	3820 6040 4220 6040
Wire Wire Line
	3820 5440 4220 5440
Wire Wire Line
	3820 5940 4220 5940
Wire Wire Line
	3820 5540 4220 5540
Wire Wire Line
	3820 5640 4220 5640
Wire Wire Line
	3820 5840 4220 5840
$Comp
L izi-memory:SDRAM_BGA-90 U1
U 1 1 5C03C5F2
P 3220 4140
F 0 "U1" H 3155 6645 59  0000 L CIB
F 1 "SDRAM_BGA-90" H 3000 6545 39  0000 L CIB
F 2 "Package_BGA:BGA-90_8.0x13.0mm_Layout2x3x15_P0.8mm" H 3220 2340 39  0001 L TIB
F 3 "" H 3220 2340 39  0000 L TIB
F 4 "IS42S32400F-7BLI" H 2955 6500 39  0000 L TIB "Manufacture Ref"
F 5 "https://www.mouser.fr/ProductDetail/ISSI/IS42S32400F-7BLI?qs=sGAEpiMZZMti5BT4iPSEnePfbwObW22xhWGgqqHaYz0%3d" H 3220 4140 39  0001 L TIB "Distributor"
	1    3220 4140
	1    0    0    -1  
$EndComp
NoConn ~ 7335 4475
NoConn ~ 7335 4375
Text Label 6985 2075 0    50   ~ 0
A12
Text Label 6985 2175 0    50   ~ 0
A11
Text Label 6985 2475 0    50   ~ 0
A8
Text Label 6985 3175 0    50   ~ 0
A1
Text Label 6985 3275 0    50   ~ 0
A0
Text Label 6985 2775 0    50   ~ 0
A5
Text Label 6985 2675 0    50   ~ 0
A6
Text Label 6985 2875 0    50   ~ 0
A4
Text Label 6985 2275 0    50   ~ 0
A10
Text Label 6985 2975 0    50   ~ 0
A3
Text Label 6985 3075 0    50   ~ 0
A2
Text Label 6985 2575 0    50   ~ 0
A7
Text Label 6985 2375 0    50   ~ 0
A9
Text Label 6985 3475 0    50   ~ 0
BA1
Text Label 6985 3575 0    50   ~ 0
BA0
Wire Wire Line
	6885 2475 7335 2475
Wire Wire Line
	6885 3875 7335 3875
Wire Wire Line
	7335 2775 6885 2775
Wire Wire Line
	7335 2575 6885 2575
Wire Wire Line
	7335 2375 6885 2375
Wire Wire Line
	7335 2175 6885 2175
Wire Wire Line
	6885 2075 7335 2075
Wire Wire Line
	6885 2275 7335 2275
Wire Wire Line
	7335 3475 6885 3475
Wire Wire Line
	6885 4125 7335 4125
Wire Wire Line
	6885 2675 7335 2675
Wire Wire Line
	7335 3775 6885 3775
Wire Wire Line
	6885 2875 7335 2875
Wire Wire Line
	6885 3575 7335 3575
Wire Wire Line
	6885 3075 7335 3075
Wire Wire Line
	6885 3275 7335 3275
Wire Wire Line
	7335 3975 6885 3975
Wire Wire Line
	7335 4225 6885 4225
Wire Wire Line
	7335 2975 6885 2975
Wire Wire Line
	7335 3175 6885 3175
Text Label 6985 4675 0    50   ~ 0
CLK
Text Label 6985 4225 0    50   ~ 0
CKE0
Text Label 6985 3775 0    50   ~ 0
WE
Text Label 6985 3875 0    50   ~ 0
CAS
Text Label 6985 3975 0    50   ~ 0
RAS
Text Label 6985 4125 0    50   ~ 0
CS0
Text Label 6985 5075 0    50   ~ 0
DQM2
Text Label 6985 4875 0    50   ~ 0
DQM0
Text Label 9385 2475 0    50   ~ 0
DQ27
Text Label 9385 2575 0    50   ~ 0
DQ26
Text Label 9385 2275 0    50   ~ 0
DQ29
Text Label 9385 2775 0    50   ~ 0
DQ24
Text Label 9385 2075 0    50   ~ 0
DQ31
Text Label 9385 2375 0    50   ~ 0
DQ28
Text Label 6985 5175 0    50   ~ 0
DQM3
Text Label 9385 2675 0    50   ~ 0
DQ25
Text Label 9385 2875 0    50   ~ 0
DQ23
Text Label 6985 4975 0    50   ~ 0
DQM1
Text Label 9385 2175 0    50   ~ 0
DQ30
Text Label 9385 3575 0    50   ~ 0
DQ16
Text Label 9385 2975 0    50   ~ 0
DQ22
Text Label 9385 3975 0    50   ~ 0
DQ12
Text Label 9385 4075 0    50   ~ 0
DQ11
Text Label 9385 4675 0    50   ~ 0
DQ5
Text Label 9385 4275 0    50   ~ 0
DQ9
Text Label 9385 4775 0    50   ~ 0
DQ4
Text Label 9385 3075 0    50   ~ 0
DQ21
Text Label 9385 3175 0    50   ~ 0
DQ20
Text Label 9385 3875 0    50   ~ 0
DQ13
Text Label 9385 4875 0    50   ~ 0
DQ3
Text Label 9385 4975 0    50   ~ 0
DQ2
Text Label 9385 4575 0    50   ~ 0
DQ6
Text Label 9385 3275 0    50   ~ 0
DQ19
Text Label 9385 4475 0    50   ~ 0
DQ7
Text Label 9385 3375 0    50   ~ 0
DQ18
Text Label 9385 3675 0    50   ~ 0
DQ15
Text Label 9385 3475 0    50   ~ 0
DQ17
Text Label 9385 4375 0    50   ~ 0
DQ8
Text Label 9385 3775 0    50   ~ 0
DQ14
Text Label 9385 4175 0    50   ~ 0
DQ10
Text Label 9385 5075 0    50   ~ 0
DQ1
Text Label 9385 5175 0    50   ~ 0
DQ0
Entry Wire Line
	9685 3175 9785 3075
Entry Wire Line
	9685 2975 9785 2875
Entry Wire Line
	9685 3775 9785 3675
Entry Wire Line
	9685 4375 9785 4275
Entry Wire Line
	9685 3975 9785 3875
Entry Wire Line
	9685 4775 9785 4675
Entry Wire Line
	9685 4675 9785 4575
Entry Wire Line
	9685 5075 9785 4975
Entry Wire Line
	9685 3375 9785 3275
Entry Wire Line
	9685 3875 9785 3775
Entry Wire Line
	9685 4175 9785 4075
Entry Wire Line
	9685 4475 9785 4375
Entry Wire Line
	9685 4575 9785 4475
Entry Wire Line
	9685 2675 9785 2575
Entry Wire Line
	9685 2075 9785 1975
Entry Wire Line
	9685 3075 9785 2975
Entry Wire Line
	9685 3475 9785 3375
Entry Wire Line
	9685 4275 9785 4175
Entry Wire Line
	9685 5175 9785 5075
Entry Wire Line
	9685 2775 9785 2675
Entry Wire Line
	9685 2275 9785 2175
Entry Wire Line
	9685 3275 9785 3175
Entry Wire Line
	9685 3675 9785 3575
Entry Wire Line
	9685 4075 9785 3975
Entry Wire Line
	9685 4875 9785 4775
Entry Wire Line
	9685 4975 9785 4875
Entry Wire Line
	9685 3575 9785 3475
Entry Wire Line
	9685 2175 9785 2075
Entry Wire Line
	9685 2375 9785 2275
Entry Wire Line
	9685 2475 9785 2375
Entry Wire Line
	9685 2575 9785 2475
Entry Wire Line
	9685 2875 9785 2775
Wire Wire Line
	9135 4475 9685 4475
Wire Wire Line
	9135 4675 9685 4675
Wire Wire Line
	9135 4575 9685 4575
Wire Wire Line
	9135 4775 9685 4775
Wire Wire Line
	9135 4875 9685 4875
Wire Wire Line
	9135 4975 9685 4975
Wire Wire Line
	9135 5075 9685 5075
Wire Wire Line
	9135 5175 9685 5175
Wire Wire Line
	9135 2175 9685 2175
Wire Wire Line
	9135 4375 9685 4375
Wire Wire Line
	9135 4175 9685 4175
Wire Wire Line
	9135 3375 9685 3375
Wire Bus Line
	5985 5725 6035 5775
Wire Wire Line
	6885 5175 7335 5175
Wire Wire Line
	6885 4975 7335 4975
Wire Wire Line
	7335 5075 6885 5075
Wire Wire Line
	7335 4875 6885 4875
Wire Wire Line
	9135 3175 9685 3175
Wire Bus Line
	9785 5725 9735 5775
Wire Wire Line
	9135 2875 9685 2875
Wire Wire Line
	9135 3675 9685 3675
Wire Wire Line
	9135 3775 9685 3775
Wire Wire Line
	9135 3975 9685 3975
Wire Wire Line
	9135 4075 9685 4075
Wire Wire Line
	9135 4275 9685 4275
Wire Wire Line
	9135 2275 9685 2275
Wire Wire Line
	9135 2675 9685 2675
Wire Wire Line
	9135 3875 9685 3875
Wire Wire Line
	9135 3275 9685 3275
Wire Wire Line
	9135 2075 9685 2075
Wire Wire Line
	6885 4675 7335 4675
Wire Wire Line
	9135 2575 9685 2575
Wire Wire Line
	9135 3475 9685 3475
Wire Wire Line
	9135 2775 9685 2775
Wire Wire Line
	9135 3575 9685 3575
Wire Wire Line
	9135 2475 9685 2475
Wire Wire Line
	9135 2375 9685 2375
Wire Wire Line
	9135 3075 9685 3075
Wire Wire Line
	9135 2975 9685 2975
Wire Bus Line
	6035 5775 9735 5775
$Comp
L izi-mcu:STM32F769NIH6 U3
U 2 1 5C03C50C
P 8235 4425
F 0 "U3" H 8145 7145 59  0000 L CIB
F 1 "STM32F769NIH6" H 8010 7020 39  0000 L CIB
F 2 "Package_BGA:TFBGA-216_13x13mm_Layout15x15_P0.8mm" H 10485 2875 39  0001 L TIB
F 3 "" H 10485 2875 39  0000 L TIB
F 4 "STM32F769NIH6" H 8005 6955 39  0000 L TIB "Manufacture Ref"
F 5 "https://www.mouser.fr/ProductDetail/STMicroelectronics/STM32F769NIH6?qs=dTJS0cRn7ogA2J674TGvTQ==" H 8285 7275 39  0001 L TIB "Distributor"
	2    8235 4425
	1    0    0    -1  
$EndComp
Wire Bus Line
	5985 1490 5985 5725
Entry Wire Line
	6785 1975 6885 2075
Entry Wire Line
	6785 3775 6885 3875
Entry Wire Line
	6785 2375 6885 2475
Entry Wire Line
	6785 2575 6885 2675
Entry Wire Line
	6785 4125 6885 4225
Entry Wire Line
	6785 2475 6885 2575
Entry Wire Line
	6785 2675 6885 2775
Entry Wire Line
	6785 3075 6885 3175
Entry Wire Line
	6785 3675 6885 3775
Entry Wire Line
	6785 3375 6885 3475
Entry Wire Line
	6785 4575 6885 4675
Entry Wire Line
	6785 3875 6885 3975
Entry Wire Line
	6785 2875 6885 2975
Entry Wire Line
	6785 4025 6885 4125
Entry Wire Line
	6785 3175 6885 3275
Entry Wire Line
	6785 2075 6885 2175
Entry Wire Line
	6785 2175 6885 2275
Entry Wire Line
	6785 2775 6885 2875
Entry Wire Line
	6785 2275 6885 2375
Entry Wire Line
	6785 3475 6885 3575
Entry Wire Line
	6785 2975 6885 3075
Entry Wire Line
	6785 4775 6885 4875
Entry Wire Line
	6785 4875 6885 4975
Entry Wire Line
	6785 4975 6885 5075
Wire Bus Line
	4330 1425 4270 1490
Wire Bus Line
	5985 1490 5920 1425
Wire Bus Line
	5920 1425 4330 1425
Entry Wire Line
	6785 5075 6885 5175
Wire Bus Line
	6725 965  6785 1015
Wire Bus Line
	2020 965  6725 965 
Wire Bus Line
	1970 1015 2020 965 
$Comp
L device:C_Small C5
U 1 1 5C03EA8D
P 1970 5540
F 0 "C5" V 1925 5380 59  0000 L CIB
F 1 "100nF" V 1930 5595 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 5540 39  0001 L TIB
F 3 "~" H 1970 5540 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 5540
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C9
U 1 1 5C03EAC7
P 1970 5740
F 0 "C9" V 1930 5580 59  0000 L CIB
F 1 "100nF" V 1930 5795 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1970 5740 39  0001 L TIB
F 3 "~" H 1970 5740 39  0001 L TIB
F 4 "MC0402B104K160CT" H 970 490 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 970 490 39  0001 L TIB "Distributor"
	1    1970 5740
	0    1    1    0   
$EndComp
Wire Bus Line
	1970 1015 1970 4535
Wire Bus Line
	4270 1490 4270 4940
Wire Bus Line
	9785 1975 9785 5725
Wire Bus Line
	6785 1015 6785 5075
$EndSCHEMATC
