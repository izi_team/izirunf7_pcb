EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 10
Title "izirunf7"
Date "2020-08-31"
Rev "A"
Comp "IZITRON"
Comment1 "www.izitron.com"
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Text Notes 6630 925  2    118  Italic 24
CONNECTORS
$Comp
L Mechanical:Fiducial FID1
U 1 1 5CE94377
P 9060 4150
F 0 "FID1" H 9145 4196 59  0000 L CIB
F 1 "Fiducial" H 9145 4105 39  0000 L CIB
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 9060 4150 39  0001 L TIB
F 3 "~" H 9060 4150 39  0001 L TIB
	1    9060 4150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID2
U 1 1 5CE9439D
P 9060 4375
F 0 "FID2" H 9145 4421 59  0000 L CIB
F 1 "Fiducial" H 9145 4330 39  0000 L CIB
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 9060 4375 39  0001 L TIB
F 3 "~" H 9060 4375 39  0001 L TIB
	1    9060 4375
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID3
U 1 1 5CE943C3
P 9060 4600
F 0 "FID3" H 9145 4646 59  0000 L CIB
F 1 "Fiducial" H 9145 4555 39  0000 L CIB
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 9060 4600 39  0001 L TIB
F 3 "~" H 9060 4600 39  0001 L TIB
	1    9060 4600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID4
U 1 1 5CE943EF
P 9060 4850
F 0 "FID4" H 9145 4896 59  0000 L CIB
F 1 "Fiducial" H 9145 4805 39  0000 L CIB
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 9060 4850 39  0001 L TIB
F 3 "~" H 9060 4850 39  0001 L TIB
	1    9060 4850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID5
U 1 1 5CE9441D
P 9060 5100
F 0 "FID5" H 9145 5146 59  0000 L CIB
F 1 "Fiducial" H 9145 5055 39  0000 L CIB
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 9060 5100 39  0001 L TIB
F 3 "~" H 9060 5100 39  0001 L TIB
	1    9060 5100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID6
U 1 1 5CE9448E
P 9060 5375
F 0 "FID6" H 9145 5421 59  0000 L CIB
F 1 "Fiducial" H 9145 5330 39  0000 L CIB
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 9060 5375 39  0001 L TIB
F 3 "~" H 9060 5375 39  0001 L TIB
	1    9060 5375
	1    0    0    -1  
$EndComp
NoConn ~ 3475 3380
NoConn ~ 3475 4480
NoConn ~ 3475 4780
NoConn ~ 3475 4980
Connection ~ 2005 2380
Connection ~ 4140 2430
NoConn ~ 3475 4180
NoConn ~ 3475 4280
NoConn ~ 3475 3280
NoConn ~ 3475 4380
NoConn ~ 3475 4580
NoConn ~ 3475 4680
NoConn ~ 3475 4880
$Comp
L conn:Conn_01x05 J2
U 1 1 5EF01E22
P 9135 2620
F 0 "J2" H 9140 3055 59  0000 C CIB
F 1 "53261-0571" H 9140 2950 39  0000 C CIB
F 2 "SamacSys_Parts:532610571TR250" H 9135 2620 50  0001 C CNN
F 3 "~" H 9135 2620 50  0001 C CNN
F 4 "53261-0571" H 9135 2620 50  0001 C CNN "REF"
F 5 "538-53261-0571" H 9135 2620 50  0001 C CNN "MOUSER"
F 6 "1125360" H 9135 2620 50  0001 C CNN "FARNELL"
F 7 "C392176" H 9135 2620 50  0001 C CNN "LCSC"
	1    9135 2620
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR052
U 1 1 5EF147E3
P 8495 2820
F 0 "#PWR052" H 8495 2570 59  0001 C CIB
F 1 "GND" V 8495 2630 39  0000 C CIB
F 2 "" H 8495 2820 39  0001 C CIB
F 3 "" H 8495 2820 39  0001 C CIB
	1    8495 2820
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR051
U 1 1 5F0389F3
P 8495 2420
F 0 "#PWR051" H 8495 2270 59  0001 C CIB
F 1 "+3V3" V 8495 2655 39  0000 C CIB
F 2 "" H 8495 2420 39  0001 C CIB
F 3 "" H 8495 2420 39  0001 C CIB
	1    8495 2420
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR049
U 1 1 5EF7E053
P 1935 2380
F 0 "#PWR049" H 1935 2230 59  0001 C CIB
F 1 "+5V" V 1935 2580 39  0000 C CIB
F 2 "" H 1935 2380 39  0001 C CIB
F 3 "" H 1935 2380 39  0001 C CIB
	1    1935 2380
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5F0365B5
P 6080 2235
F 0 "H1" H 6245 2310 59  0000 C CIB
F 1 "MountingHole" H 6385 2175 39  0000 C CIB
F 2 "Mounting_Holes:MountingHole_2.7mm_M2.5_Pad_Via" H 6080 2235 50  0001 C CNN
F 3 "~" H 6080 2235 50  0001 C CNN
	1    6080 2235
	1    0    0    -1  
$EndComp
$Comp
L izi-connectors:M_2 J1
U 1 1 5EEF272B
P 3075 4030
F 0 "J1" H 3075 6016 59  0000 C CIB
F 1 "M 2" H 3075 5927 39  0000 C CIB
F 2 "MyFootPrint:M.2_PCB" H 3075 4030 39  0001 C CIB
F 3 "" H 3075 4030 39  0001 C CIB
F 4 "2199119-4" H 3075 5854 39  0000 C CIB "REF"
F 5 "571-2199119-4" H 3075 4030 39  0001 C CIB "MOUSER"
F 6 "2377468" H 3075 4030 39  0001 C CIB "FARNELL"
F 7 "NC(only footprint)" H 3075 4030 39  0001 C CIB "ASSAMBLED"
	1    3075 4030
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR050
U 1 1 5F071637
P 4265 2430
F 0 "#PWR050" H 4265 2180 59  0001 C CIB
F 1 "GND" V 4265 2240 39  0000 C CIB
F 2 "" H 4265 2430 39  0001 C CIB
F 3 "" H 4265 2430 39  0001 C CIB
	1    4265 2430
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2675 2330 2005 2330
Wire Wire Line
	2000 4230 2675 4230
Wire Wire Line
	3475 2780 4140 2780
Wire Wire Line
	2005 3030 2675 3030
Wire Wire Line
	2675 3230 2005 3230
Wire Wire Line
	2000 4430 2675 4430
Wire Wire Line
	3475 2580 4140 2580
Wire Wire Line
	4140 2980 3475 2980
Wire Wire Line
	8495 2420 8935 2420
Wire Wire Line
	2000 4130 2675 4130
Wire Wire Line
	8490 2620 8935 2620
Wire Wire Line
	1995 4630 2675 4630
Wire Wire Line
	2675 2530 2005 2530
Wire Wire Line
	2675 4730 1995 4730
Wire Wire Line
	1995 4830 2675 4830
Wire Wire Line
	2005 3630 2675 3630
Wire Wire Line
	2675 4530 2000 4530
Wire Wire Line
	4140 2380 4140 2430
Wire Wire Line
	1935 2380 2005 2380
Wire Wire Line
	2675 4930 1995 4930
Wire Wire Line
	4140 3080 3475 3080
Wire Wire Line
	2675 5030 1995 5030
Wire Wire Line
	2675 5130 1995 5130
Wire Wire Line
	3475 2880 4140 2880
Wire Wire Line
	2005 3730 2675 3730
Wire Wire Line
	8490 2520 8935 2520
Wire Wire Line
	2675 2930 2005 2930
Wire Wire Line
	4145 3180 3475 3180
Wire Wire Line
	8935 2820 8495 2820
Wire Wire Line
	2005 2380 2005 2430
Wire Wire Line
	8935 2720 8490 2720
Wire Wire Line
	3475 2380 4140 2380
Wire Wire Line
	2675 3430 2005 3430
Wire Wire Line
	4265 2430 4140 2430
Wire Wire Line
	3475 2680 4140 2680
Wire Wire Line
	2000 3830 2675 3830
Wire Wire Line
	2000 4330 2675 4330
Wire Wire Line
	2005 2330 2005 2380
Wire Wire Line
	2005 2730 2675 2730
Wire Wire Line
	2005 2630 2675 2630
Wire Wire Line
	2005 3130 2675 3130
Wire Wire Line
	2000 3930 2675 3930
Wire Wire Line
	4140 2480 3475 2480
Wire Wire Line
	2005 3330 2675 3330
Wire Wire Line
	4140 2430 4140 2480
Wire Wire Line
	2005 2430 2675 2430
Wire Wire Line
	2005 2830 2675 2830
Wire Wire Line
	2675 4030 2000 4030
Text HLabel 2005 3030 0    50   Output ~ 0
SPI1_MISO
Text HLabel 2005 3330 0    50   Input ~ 0
SPI1_CS1
Text HLabel 2005 3430 0    50   Input ~ 0
SPI1_CS2
Text HLabel 2005 3230 0    50   Input ~ 0
SPI1_CS0
Text HLabel 2005 3730 0    50   Output ~ 0
ADC1
Text HLabel 2000 4130 0    50   Input ~ 0
PWM3
Text HLabel 2005 2530 0    50   Input ~ 0
USART1_TX
Text HLabel 2005 2630 0    50   Output ~ 0
USART1_RX
Text HLabel 2005 2730 0    50   Input ~ 0
I2C1_SCL
Text HLabel 2005 2830 0    50   BiDi ~ 0
I2C1_SDA
Text HLabel 2005 2930 0    50   Input ~ 0
SPI1_SCK
Text HLabel 2005 3630 0    50   Output ~ 0
ADC0
Text HLabel 2000 3830 0    50   Input ~ 0
PWM0
Text HLabel 2000 3930 0    50   Input ~ 0
PWM1
Text HLabel 2005 3130 0    50   Input ~ 0
SPI1_MOSI
Text HLabel 2000 4030 0    50   Input ~ 0
PWM2
Text HLabel 8490 2620 0    50   BiDi ~ 0
SWD_SWDIO
Text Notes 3745 1880 2    118  Italic 16
M2 CONNECTOR
Text HLabel 1995 4730 0    50   BiDi ~ 0
IO3
Text HLabel 4140 2580 2    50   Input ~ 0
NRST
Text HLabel 4140 2880 2    50   UnSpc ~ 0
CAN_H
Text HLabel 1995 4830 0    50   BiDi ~ 0
IO4
Text HLabel 1995 4630 0    50   BiDi ~ 0
IO2
Text HLabel 4140 3080 2    50   BiDi ~ 0
USB_DM
Text HLabel 2000 4230 0    50   Input ~ 0
PWM4
Text HLabel 4140 2680 2    50   Input ~ 0
BOOT
Text HLabel 1995 4930 0    50   BiDi ~ 0
IO5
Text HLabel 2000 4430 0    50   BiDi ~ 0
IO0
Text HLabel 2000 4530 0    50   BiDi ~ 0
IO1
Text HLabel 4140 2980 2    50   UnSpc ~ 0
CAN_L
Text HLabel 4140 2780 2    50   Input ~ 0
WKUP
Text Notes 9200 1845 2    118  Italic 16
SWD DEBUG
Text HLabel 8490 2520 0    50   Output ~ 0
SWD_SWCLK
Text HLabel 2000 4330 0    50   Input ~ 0
PWM5
Text Notes 6775 1845 2    118  Italic 16
Mounting Hole
Text HLabel 8490 2720 0    50   Input ~ 0
NRST
Text HLabel 4150 3780 2    50   BiDi ~ 0
ETH_RXP
Text HLabel 4150 3980 2    50   BiDi ~ 0
ETH_LED_G
Text HLabel 4165 5580 2    50   BiDi ~ 0
IO19
Text HLabel 4165 5280 2    50   BiDi ~ 0
IO16
Text HLabel 1995 5230 0    50   BiDi ~ 0
IO8
Text HLabel 1995 5330 0    50   BiDi ~ 0
IO9
Text HLabel 1995 5430 0    50   BiDi ~ 0
IO10
Text HLabel 4165 5480 2    50   BiDi ~ 0
IO18
Text HLabel 1995 5030 0    50   BiDi ~ 0
IO6
Text HLabel 1995 5130 0    50   BiDi ~ 0
IO7
Text HLabel 4165 5680 2    50   BiDi ~ 0
IO20
Text HLabel 1995 5630 0    50   BiDi ~ 0
IO12
Text HLabel 4150 3580 2    50   BiDi ~ 0
ETH_TXP
Text HLabel 4150 3680 2    50   BiDi ~ 0
ETH_TXN
Text HLabel 4165 5180 2    50   BiDi ~ 0
IO15
Text HLabel 4150 3880 2    50   BiDi ~ 0
ETH_RXN
Text HLabel 4145 3180 2    50   BiDi ~ 0
USB_DP
Text HLabel 1995 5730 0    50   BiDi ~ 0
IO13
Text HLabel 4165 5080 2    50   BiDi ~ 0
IO14
Text HLabel 1995 5530 0    50   BiDi ~ 0
IO11
Text HLabel 4165 5380 2    50   BiDi ~ 0
IO17
Wire Wire Line
	3475 5480 4165 5480
Wire Wire Line
	2675 5430 1995 5430
Wire Wire Line
	2675 5530 1995 5530
Wire Wire Line
	3475 5580 4165 5580
Wire Wire Line
	2675 5730 1995 5730
Wire Wire Line
	3475 5080 4165 5080
Wire Wire Line
	3475 3780 4150 3780
Wire Wire Line
	2675 5330 1995 5330
Wire Wire Line
	3475 5180 4165 5180
Wire Wire Line
	3475 3880 4150 3880
Wire Wire Line
	2675 5230 1995 5230
Wire Wire Line
	3475 3980 4150 3980
Wire Wire Line
	3475 3680 4150 3680
Wire Wire Line
	3475 5680 4165 5680
Wire Wire Line
	2675 5630 1995 5630
Wire Wire Line
	3475 3580 4150 3580
Wire Wire Line
	3475 4080 4150 4080
Wire Wire Line
	3475 5280 4165 5280
Wire Wire Line
	3475 5380 4165 5380
Text HLabel 4150 4080 2    50   BiDi ~ 0
ETH_LED_Y
$EndSCHEMATC
