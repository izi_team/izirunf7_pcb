EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 10
Title "izirunf7"
Date "2020-08-31"
Rev "A"
Comp "IZITRON"
Comment1 "www.izitron.com"
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
$Comp
L izi-mcu:STM32F769NIH6 U3
U 1 1 5C03E14C
P 3875 3875
F 0 "U3" H 3800 6585 59  0000 L CIB
F 1 "STM32F769NIH6" H 3670 6465 39  0000 L CIB
F 2 "Package_BGA:TFBGA-216_13x13mm_Layout15x15_P0.8mm" H 1475 3325 39  0001 L TIB
F 3 "" H 1475 3325 39  0000 L TIB
F 4 "STM32F769NIH6 " H 3650 6410 39  0000 L TIB "Manufacture Ref"
F 5 "https://www.mouser.fr/ProductDetail/STMicroelectronics/STM32F769NIH6?qs=dTJS0cRn7ogA2J674TGvTQ==" H 3875 3875 39  0001 L TIB "Distributor"
	1    3875 3875
	1    0    0    -1  
$EndComp
$Comp
L izi-mcu:STM32F769NIH6 U3
U 7 1 5C2B5E9F
P 8575 2775
F 0 "U3" H 8485 4365 59  0000 L CIB
F 1 "STM32F769NIH6" H 8350 4220 39  0000 L CIB
F 2 "Package_BGA:TFBGA-216_13x13mm_Layout15x15_P0.8mm" H 9475 1575 39  0001 L TIB
F 3 "" H 9475 1575 39  0000 L TIB
F 4 "STM32F769NIH6" H 8345 4160 39  0000 L TIB "Manufacture Ref"
F 5 "https://www.mouser.fr/ProductDetail/STMicroelectronics/STM32F769NIH6?qs=dTJS0cRn7ogA2J674TGvTQ==" H 8625 5625 39  0001 L TIB "Distributor"
	7    8575 2775
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C65
U 1 1 5C261B4A
P 10200 3925
F 0 "C65" V 10150 3950 59  0000 L CIB
F 1 "2.2uF" V 10250 3950 39  0000 L CIB
F 2 "Capacitors_SMD:C_0603" H 10200 3925 39  0001 L TIB
F 3 "~" H 10200 3925 39  0001 L TIB
F 4 "https://fr.farnell.com/multicomp/mc0603f225z100ct/condensateur-2-2-f-10v-y5v-0603/dp/1759400" H 0   0   39  0001 L TIB "Distributor"
F 5 "MC0603F225Z100CT" H 0   0   39  0001 L TIB "Manufacture Ref"
	1    10200 3925
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C64
U 1 1 5C261BAF
P 9700 3825
F 0 "C64" V 9650 3875 59  0000 L CIB
F 1 "2.2uF" V 9750 3875 39  0000 L CIB
F 2 "Capacitors_SMD:C_0603" H 9700 3825 39  0001 L TIB
F 3 "~" H 9700 3825 39  0001 L TIB
F 4 "https://fr.farnell.com/multicomp/mc0603f225z100ct/condensateur-2-2-f-10v-y5v-0603/dp/1759400" H 0   0   39  0001 L TIB "Distributor"
F 5 "MC0603F225Z100CT" H 0   0   39  0001 L TIB "Manufacture Ref"
	1    9700 3825
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR065
U 1 1 5C35D775
P 9950 2650
F 0 "#PWR065" H 9950 2400 59  0001 L CIB
F 1 "GND" H 9890 2485 39  0000 L CIB
F 2 "" H 9950 2650 39  0001 L TIB
F 3 "" H 9950 2650 39  0001 L TIB
	1    9950 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9425 1575 9675 1575
Wire Wire Line
	9675 1575 9675 1675
Wire Wire Line
	9675 3625 9425 3625
Wire Wire Line
	9425 3475 9675 3475
Connection ~ 9675 3475
Wire Wire Line
	9675 3475 9675 3625
Wire Wire Line
	9425 3275 9675 3275
Connection ~ 9675 3275
Wire Wire Line
	9675 3275 9675 3475
Wire Wire Line
	9425 3175 9675 3175
Connection ~ 9675 3175
Wire Wire Line
	9675 3175 9675 3275
Wire Wire Line
	9425 3075 9675 3075
Connection ~ 9675 3075
Wire Wire Line
	9675 3075 9675 3175
Wire Wire Line
	9425 2975 9675 2975
Connection ~ 9675 2975
Wire Wire Line
	9675 2975 9675 3075
Wire Wire Line
	9425 2875 9675 2875
Connection ~ 9675 2875
Wire Wire Line
	9675 2875 9675 2975
Wire Wire Line
	9425 2775 9675 2775
Connection ~ 9675 2775
Wire Wire Line
	9675 2775 9675 2875
Wire Wire Line
	9425 2675 9675 2675
Connection ~ 9675 2675
Wire Wire Line
	9675 2675 9675 2775
Wire Wire Line
	9425 2575 9675 2575
Connection ~ 9675 2575
Wire Wire Line
	9675 2575 9675 2675
Wire Wire Line
	9425 2475 9675 2475
Connection ~ 9675 2475
Wire Wire Line
	9675 2475 9675 2575
Wire Wire Line
	9425 2375 9675 2375
Connection ~ 9675 2375
Wire Wire Line
	9675 2375 9675 2475
Wire Wire Line
	9425 2275 9675 2275
Connection ~ 9675 2275
Wire Wire Line
	9675 2275 9675 2375
Wire Wire Line
	9425 2175 9675 2175
Connection ~ 9675 2175
Wire Wire Line
	9675 2175 9675 2275
Wire Wire Line
	9425 2075 9675 2075
Connection ~ 9675 2075
Wire Wire Line
	9675 2075 9675 2175
Wire Wire Line
	9425 1975 9675 1975
Connection ~ 9675 1975
Wire Wire Line
	9675 1975 9675 2075
Wire Wire Line
	9425 1875 9675 1875
Connection ~ 9675 1875
Wire Wire Line
	9675 1875 9675 1975
Wire Wire Line
	9425 1775 9675 1775
Connection ~ 9675 1775
Wire Wire Line
	9675 1775 9675 1875
Wire Wire Line
	9425 1675 9675 1675
Connection ~ 9675 1675
Wire Wire Line
	9675 1675 9675 1775
Wire Wire Line
	9675 2575 9950 2575
Wire Wire Line
	9950 2575 9950 2650
$Comp
L device:R_Small R49
U 1 1 5C364518
P 7375 4300
F 0 "R49" H 7434 4346 59  0000 L CIB
F 1 "10k" H 7434 4255 39  0000 L CIB
F 2 "Resistor_SMD:R_0402_1005Metric" H 7375 4300 39  0001 L TIB
F 3 "~" H 7375 4300 39  0001 L TIB
	1    7375 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR071
U 1 1 5C3645CB
P 7375 4575
F 0 "#PWR071" H 7375 4325 59  0001 L CIB
F 1 "GND" H 7315 4400 39  0000 L CIB
F 2 "" H 7375 4575 39  0001 L TIB
F 3 "" H 7375 4575 39  0001 L TIB
	1    7375 4575
	1    0    0    -1  
$EndComp
Wire Wire Line
	7675 4075 7375 4075
Wire Wire Line
	7375 4075 7375 4200
Wire Wire Line
	7375 4400 7375 4525
$Comp
L power:GND #PWR069
U 1 1 5C261C14
P 11050 3925
F 0 "#PWR069" H 11050 3675 59  0001 L CIB
F 1 "GND" H 10990 3760 39  0000 L CIB
F 2 "" H 11050 3925 39  0001 L TIB
F 3 "" H 11050 3925 39  0001 L TIB
	1    11050 3925
	1    0    0    -1  
$EndComp
Text HLabel 10075 4075 2    50   Input ~ 0
VBAT_RTC
Wire Wire Line
	9425 4075 9575 4075
$Comp
L device:C_Small C66
U 1 1 5C374D00
P 9575 4325
F 0 "C66" H 9600 4400 59  0000 L CIB
F 1 "100nF" H 9600 4250 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 9575 4325 39  0001 L TIB
F 3 "~" H 9575 4325 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    9575 4325
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR072
U 1 1 5C374D36
P 9575 4650
F 0 "#PWR072" H 9575 4400 59  0001 L CIB
F 1 "GND" H 9515 4490 39  0000 L CIB
F 2 "" H 9575 4650 39  0001 L TIB
F 3 "" H 9575 4650 39  0001 L TIB
	1    9575 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9575 4075 9575 4225
Connection ~ 9575 4075
Wire Wire Line
	9575 4425 9575 4650
Text Notes 10550 4545 2    39   Italic 8
Ceramic Cap\n  Low ESR
Wire Wire Line
	7675 3975 7225 3975
Wire Wire Line
	7225 3975 7225 4525
Wire Wire Line
	7225 4525 7375 4525
Connection ~ 7375 4525
Wire Wire Line
	7375 4525 7375 4575
$Comp
L device:C_Small C69
U 1 1 5C38CD4F
P 8375 5925
F 0 "C69" H 8400 6000 59  0000 L CIB
F 1 "100nF" H 8400 5850 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 8375 5925 39  0001 L TIB
F 3 "~" H 8375 5925 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    8375 5925
	1    0    0    -1  
$EndComp
$Comp
L device:CP_Small CP3
U 1 1 5C38CE35
P 8025 5925
F 0 "CP3" H 8050 6000 59  0000 L CIB
F 1 "1uF" H 8050 5850 39  0000 L CIB
F 2 "Capacitor_Tantalum_SMD:CP_EIA-1608-08_AVX-J" H 8025 5925 39  0001 L TIB
F 3 "~" H 8025 5925 39  0001 L TIB
F 4 "https://fr.farnell.com/avx/0603yd105kat2a/condensateur-1-f-16v-10-x5r-0603/dp/1327680" H 0   0   39  0001 L TIB "Distributor"
F 5 "603YD105KAT2A" H 0   0   39  0001 L TIB "Manufacture Ref"
	1    8025 5925
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR068
U 1 1 5C390319
P 6550 3775
F 0 "#PWR068" H 6550 3625 59  0001 L CIB
F 1 "+3V3" H 6470 3920 39  0000 L CIB
F 2 "" H 6550 3775 39  0001 L TIB
F 3 "" H 6550 3775 39  0001 L TIB
	1    6550 3775
	1    0    0    -1  
$EndComp
Wire Wire Line
	7675 1575 7450 1575
Wire Wire Line
	8025 5825 8025 5700
Wire Wire Line
	8375 5825 8375 5700
$Comp
L power:GND #PWR079
U 1 1 5C39452F
P 8200 6200
F 0 "#PWR079" H 8200 5950 59  0001 L CIB
F 1 "GND" H 8205 6027 39  0000 L CIB
F 2 "" H 8200 6200 39  0001 L TIB
F 3 "" H 8200 6200 39  0001 L TIB
	1    8200 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8025 6025 8025 6150
Wire Wire Line
	8025 6150 8200 6150
Wire Wire Line
	8375 6150 8375 6025
Wire Wire Line
	8200 6200 8200 6150
Connection ~ 8200 6150
Wire Wire Line
	8200 6150 8375 6150
Wire Wire Line
	7450 1575 7450 1675
Wire Wire Line
	6825 3875 7675 3875
Text Label 7425 3875 2    50   ~ 0
VREF+
Text Label 8350 5700 2    50   ~ 0
VREF+
Wire Wire Line
	8025 5700 8375 5700
Wire Wire Line
	7125 3775 7675 3775
Wire Wire Line
	6925 3775 6825 3775
Wire Wire Line
	6825 3775 6825 3875
Text Label 7400 3775 2    50   ~ 0
VDDA
Text Label 9075 5700 2    50   ~ 0
VDDA
$Comp
L device:C_Small C71
U 1 1 5C3B8937
P 9175 5925
F 0 "C71" H 9200 6000 59  0000 L CIB
F 1 "100nF" H 9200 5850 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 9175 5925 39  0001 L TIB
F 3 "~" H 9175 5925 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    9175 5925
	1    0    0    -1  
$EndComp
$Comp
L device:CP_Small CP4
U 1 1 5C3B893D
P 8825 5925
F 0 "CP4" H 8850 6000 59  0000 L CIB
F 1 "1uF" H 8850 5850 39  0000 L CIB
F 2 "Capacitor_Tantalum_SMD:CP_EIA-1608-08_AVX-J" H 8825 5925 39  0001 L TIB
F 3 "~" H 8825 5925 39  0001 L TIB
F 4 "https://fr.farnell.com/avx/0603yd105kat2a/condensateur-1-f-16v-10-x5r-0603/dp/1327680" H 0   0   39  0001 L TIB "Distributor"
F 5 "603YD105KAT2A" H 0   0   39  0001 L TIB "Manufacture Ref"
	1    8825 5925
	1    0    0    -1  
$EndComp
Wire Wire Line
	8825 5825 8825 5700
Wire Wire Line
	9175 5825 9175 5700
$Comp
L power:GND #PWR080
U 1 1 5C3B8945
P 9000 6200
F 0 "#PWR080" H 9000 5950 59  0001 L CIB
F 1 "GND" H 9005 6027 39  0000 L CIB
F 2 "" H 9000 6200 39  0001 L TIB
F 3 "" H 9000 6200 39  0001 L TIB
	1    9000 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8825 6025 8825 6150
Wire Wire Line
	8825 6150 9000 6150
Wire Wire Line
	9175 6150 9175 6025
Wire Wire Line
	9000 6200 9000 6150
Connection ~ 9000 6150
Wire Wire Line
	9000 6150 9175 6150
Wire Wire Line
	8825 5700 9175 5700
Wire Wire Line
	7675 3375 7450 3375
Wire Wire Line
	7675 3275 7450 3275
Connection ~ 7450 3275
Wire Wire Line
	7450 3275 7450 3375
Wire Wire Line
	7675 3175 7450 3175
Connection ~ 7450 3175
Wire Wire Line
	7450 3175 7450 3275
Wire Wire Line
	7675 3075 7450 3075
Connection ~ 7450 3075
Wire Wire Line
	7450 3075 7450 3175
$Comp
L power:+3V3 #PWR064
U 1 1 5C3CA024
P 7150 2475
F 0 "#PWR064" H 7150 2325 59  0001 L CIB
F 1 "+3V3" H 7070 2630 39  0000 L CIB
F 2 "" H 7150 2475 39  0001 L TIB
F 3 "" H 7150 2475 39  0001 L TIB
	1    7150 2475
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3775 6825 3775
Connection ~ 6825 3775
Wire Wire Line
	7675 2975 7450 2975
Connection ~ 7450 2975
Wire Wire Line
	7450 2975 7450 3075
Wire Wire Line
	7675 2875 7450 2875
Connection ~ 7450 2875
Wire Wire Line
	7450 2875 7450 2975
Wire Wire Line
	7675 2775 7450 2775
Connection ~ 7450 2775
Wire Wire Line
	7450 2775 7450 2875
Wire Wire Line
	7675 2675 7450 2675
Connection ~ 7450 2675
Wire Wire Line
	7450 2675 7450 2775
Wire Wire Line
	7675 2575 7450 2575
Connection ~ 7450 2575
Wire Wire Line
	7450 2575 7450 2675
Wire Wire Line
	7675 2475 7450 2475
Connection ~ 7450 2475
Wire Wire Line
	7450 2475 7450 2575
Wire Wire Line
	7675 2375 7450 2375
Connection ~ 7450 2375
Wire Wire Line
	7450 2375 7450 2475
Wire Wire Line
	7675 2275 7450 2275
Connection ~ 7450 2275
Wire Wire Line
	7450 2275 7450 2375
Wire Wire Line
	7675 2175 7450 2175
Connection ~ 7450 2175
Wire Wire Line
	7450 2175 7450 2275
Wire Wire Line
	7675 2075 7450 2075
Connection ~ 7450 2075
Wire Wire Line
	7450 2075 7450 2175
Wire Wire Line
	7675 1975 7450 1975
Connection ~ 7450 1975
Wire Wire Line
	7450 1975 7450 2075
Wire Wire Line
	7675 1875 7450 1875
Connection ~ 7450 1875
Wire Wire Line
	7450 1875 7450 1975
Wire Wire Line
	7675 1775 7450 1775
Connection ~ 7450 1775
Wire Wire Line
	7450 1775 7450 1875
Wire Wire Line
	7675 1675 7450 1675
Connection ~ 7450 1675
Wire Wire Line
	7450 1675 7450 1775
Wire Wire Line
	7150 2475 7150 2575
Wire Wire Line
	7150 2575 7450 2575
Text Label 7275 3525 2    50   ~ 0
VDD12DSI
Wire Wire Line
	7675 3475 7450 3475
Wire Wire Line
	7450 3475 7450 3525
Wire Wire Line
	7450 3575 7675 3575
Wire Wire Line
	7275 3525 7450 3525
Connection ~ 7450 3525
Wire Wire Line
	7450 3525 7450 3575
$Comp
L device:C_Small C73
U 1 1 5C41DF32
P 9925 5925
F 0 "C73" H 9950 6000 59  0000 L CIB
F 1 "100nF" H 9950 5850 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 9925 5925 39  0001 L TIB
F 3 "~" H 9925 5925 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    9925 5925
	1    0    0    -1  
$EndComp
Wire Wire Line
	9575 5825 9575 5700
Wire Wire Line
	9925 5825 9925 5700
$Comp
L power:GND #PWR081
U 1 1 5C41DF40
P 9750 6200
F 0 "#PWR081" H 9750 5950 59  0001 L CIB
F 1 "GND" H 9755 6027 39  0000 L CIB
F 2 "" H 9750 6200 39  0001 L TIB
F 3 "" H 9750 6200 39  0001 L TIB
	1    9750 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9575 6025 9575 6150
Wire Wire Line
	9575 6150 9750 6150
Wire Wire Line
	9925 6150 9925 6025
Wire Wire Line
	9750 6200 9750 6150
Connection ~ 9750 6150
Wire Wire Line
	9750 6150 9925 6150
Wire Wire Line
	9575 5700 9925 5700
Text Label 9925 5700 2    50   ~ 0
VDD12DSI
$Comp
L device:C_Small C72
U 1 1 5C429C13
P 9575 5925
F 0 "C72" H 9600 6000 59  0000 L CIB
F 1 "2.2uF" H 9600 5850 39  0000 L CIB
F 2 "Capacitors_SMD:C_0603" H 9575 5925 39  0001 L TIB
F 3 "~" H 9575 5925 39  0001 L TIB
F 4 "https://fr.farnell.com/multicomp/mc0603f225z100ct/condensateur-2-2-f-10v-y5v-0603/dp/1759400" H 0   0   39  0001 L TIB "Distributor"
F 5 "MC0603F225Z100CT" H 0   0   39  0001 L TIB "Manufacture Ref"
	1    9575 5925
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C83
U 1 1 5C430008
P 3600 7250
F 0 "C83" H 3625 7325 59  0000 L CIB
F 1 "100nF" H 3625 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 3600 7250 39  0001 L TIB
F 3 "~" H 3600 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    3600 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C84
U 1 1 5C430048
P 3900 7250
F 0 "C84" H 3925 7325 59  0000 L CIB
F 1 "100nF" H 3925 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 3900 7250 39  0001 L TIB
F 3 "~" H 3900 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    3900 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C85
U 1 1 5C430239
P 4175 7250
F 0 "C85" H 4200 7325 59  0000 L CIB
F 1 "100nF" H 4200 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 4175 7250 39  0001 L TIB
F 3 "~" H 4175 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    4175 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C86
U 1 1 5C430271
P 4450 7250
F 0 "C86" H 4475 7325 59  0000 L CIB
F 1 "100nF" H 4475 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 4450 7250 39  0001 L TIB
F 3 "~" H 4450 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    4450 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C87
U 1 1 5C435F40
P 4725 7250
F 0 "C87" H 4750 7325 59  0000 L CIB
F 1 "100nF" H 4750 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 4725 7250 39  0001 L TIB
F 3 "~" H 4725 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    4725 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C88
U 1 1 5C435F46
P 5025 7250
F 0 "C88" H 5050 7325 59  0000 L CIB
F 1 "100nF" H 5050 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 5025 7250 39  0001 L TIB
F 3 "~" H 5025 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    5025 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C89
U 1 1 5C435F4C
P 5300 7250
F 0 "C89" H 5325 7325 59  0000 L CIB
F 1 "100nF" H 5325 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 5300 7250 39  0001 L TIB
F 3 "~" H 5300 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    5300 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C90
U 1 1 5C435F52
P 5575 7250
F 0 "C90" H 5600 7325 59  0000 L CIB
F 1 "100nF" H 5600 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 5575 7250 39  0001 L TIB
F 3 "~" H 5575 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    5575 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C74
U 1 1 5C43BC7E
P 1075 7250
F 0 "C74" H 1100 7325 59  0000 L CIB
F 1 "100nF" H 1100 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1075 7250 39  0001 L TIB
F 3 "~" H 1075 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    1075 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C75
U 1 1 5C43BC84
P 1375 7250
F 0 "C75" H 1400 7325 59  0000 L CIB
F 1 "100nF" H 1400 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1375 7250 39  0001 L TIB
F 3 "~" H 1375 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    1375 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C76
U 1 1 5C43BC8A
P 1650 7250
F 0 "C76" H 1675 7325 59  0000 L CIB
F 1 "100nF" H 1675 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1650 7250 39  0001 L TIB
F 3 "~" H 1650 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    1650 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C77
U 1 1 5C43BC90
P 1925 7250
F 0 "C77" H 1950 7325 59  0000 L CIB
F 1 "100nF" H 1950 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1925 7250 39  0001 L TIB
F 3 "~" H 1925 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    1925 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C78
U 1 1 5C43BC96
P 2200 7250
F 0 "C78" H 2225 7325 59  0000 L CIB
F 1 "100nF" H 2225 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 2200 7250 39  0001 L TIB
F 3 "~" H 2200 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    2200 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C79
U 1 1 5C43BC9C
P 2500 7250
F 0 "C79" H 2525 7325 59  0000 L CIB
F 1 "100nF" H 2525 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 2500 7250 39  0001 L TIB
F 3 "~" H 2500 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    2500 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C80
U 1 1 5C43BCA2
P 2775 7250
F 0 "C80" H 2800 7325 59  0000 L CIB
F 1 "100nF" H 2800 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 2775 7250 39  0001 L TIB
F 3 "~" H 2775 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    2775 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C81
U 1 1 5C43BCA8
P 3050 7250
F 0 "C81" H 3075 7325 59  0000 L CIB
F 1 "100nF" H 3075 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 3050 7250 39  0001 L TIB
F 3 "~" H 3050 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    3050 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C91
U 1 1 5C44D384
P 5850 7250
F 0 "C91" H 5875 7325 59  0000 L CIB
F 1 "100nF" H 5875 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 5850 7250 39  0001 L TIB
F 3 "~" H 5850 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    5850 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C82
U 1 1 5C44D38A
P 3325 7250
F 0 "C82" H 3350 7325 59  0000 L CIB
F 1 "100nF" H 3350 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 3325 7250 39  0001 L TIB
F 3 "~" H 3325 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    3325 7250
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C92
U 1 1 5C452F6E
P 6175 7250
F 0 "C92" H 6200 7325 59  0000 L CIB
F 1 "100nF" H 6200 7175 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 6175 7250 39  0001 L TIB
F 3 "~" H 6175 7250 39  0001 L TIB
F 4 "MC0402B104K160CT" H 0   0   39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H 0   0   39  0001 L TIB "Distributor"
	1    6175 7250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR083
U 1 1 5C453268
P 3600 7450
F 0 "#PWR083" H 3600 7200 59  0001 L CIB
F 1 "GND" H 3605 7277 39  0000 L CIB
F 2 "" H 3600 7450 39  0001 L TIB
F 3 "" H 3600 7450 39  0001 L TIB
	1    3600 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 7350 3600 7450
Wire Wire Line
	3600 7450 3900 7450
Wire Wire Line
	5025 7450 5300 7450
Wire Wire Line
	6175 7450 6175 7350
Connection ~ 5025 7450
Wire Wire Line
	3900 7350 3900 7450
Connection ~ 3900 7450
Wire Wire Line
	3900 7450 4175 7450
Wire Wire Line
	4175 7350 4175 7450
Connection ~ 4175 7450
Wire Wire Line
	4175 7450 4450 7450
Wire Wire Line
	4450 7350 4450 7450
Connection ~ 4450 7450
Wire Wire Line
	4450 7450 4725 7450
Wire Wire Line
	4725 7350 4725 7450
Connection ~ 4725 7450
Wire Wire Line
	4725 7450 5025 7450
Wire Wire Line
	5025 7350 5025 7450
Wire Wire Line
	5300 7350 5300 7450
Connection ~ 5300 7450
Wire Wire Line
	5300 7450 5575 7450
Wire Wire Line
	5575 7350 5575 7450
Connection ~ 5575 7450
Wire Wire Line
	5575 7450 5850 7450
Wire Wire Line
	5850 7350 5850 7450
Connection ~ 5850 7450
Wire Wire Line
	5850 7450 6175 7450
$Comp
L power:+3V3 #PWR082
U 1 1 5C49D341
P 3600 7025
F 0 "#PWR082" H 3600 6875 59  0001 L CIB
F 1 "+3V3" H 3615 7198 39  0000 L CIB
F 2 "" H 3600 7025 39  0001 L TIB
F 3 "" H 3600 7025 39  0001 L TIB
	1    3600 7025
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 7150 3600 7025
Wire Wire Line
	3600 7025 3900 7025
Wire Wire Line
	5025 7025 5300 7025
Wire Wire Line
	6175 7025 6175 7150
Connection ~ 5025 7025
Wire Wire Line
	3900 7150 3900 7025
Connection ~ 3900 7025
Wire Wire Line
	3900 7025 4175 7025
Wire Wire Line
	4175 7150 4175 7025
Connection ~ 4175 7025
Wire Wire Line
	4175 7025 4450 7025
Wire Wire Line
	4450 7150 4450 7025
Connection ~ 4450 7025
Wire Wire Line
	4450 7025 4725 7025
Wire Wire Line
	4725 7150 4725 7025
Connection ~ 4725 7025
Wire Wire Line
	4725 7025 5025 7025
Wire Wire Line
	5025 7150 5025 7025
Wire Wire Line
	5300 7150 5300 7025
Connection ~ 5300 7025
Wire Wire Line
	5300 7025 5575 7025
Wire Wire Line
	5575 7150 5575 7025
Connection ~ 5575 7025
Wire Wire Line
	5575 7025 5850 7025
Wire Wire Line
	5850 7150 5850 7025
Connection ~ 5850 7025
Wire Wire Line
	5850 7025 6175 7025
Connection ~ 3600 7025
Connection ~ 3600 7450
Wire Wire Line
	1075 7350 1075 7450
Wire Wire Line
	1075 7450 1375 7450
Wire Wire Line
	3600 7025 3325 7025
Wire Wire Line
	1075 7025 1075 7150
Wire Wire Line
	1375 7150 1375 7025
Connection ~ 1375 7025
Wire Wire Line
	1375 7025 1075 7025
Wire Wire Line
	1375 7350 1375 7450
Connection ~ 1375 7450
Wire Wire Line
	1375 7450 1650 7450
Wire Wire Line
	1650 7150 1650 7025
Connection ~ 1650 7025
Wire Wire Line
	1650 7025 1375 7025
Wire Wire Line
	1650 7350 1650 7450
Connection ~ 1650 7450
Wire Wire Line
	1650 7450 1925 7450
Wire Wire Line
	1925 7150 1925 7025
Connection ~ 1925 7025
Wire Wire Line
	1925 7025 1650 7025
Wire Wire Line
	1925 7350 1925 7450
Connection ~ 1925 7450
Wire Wire Line
	1925 7450 2200 7450
Wire Wire Line
	2200 7150 2200 7025
Connection ~ 2200 7025
Wire Wire Line
	2200 7025 1925 7025
Wire Wire Line
	2200 7350 2200 7450
Connection ~ 2200 7450
Wire Wire Line
	2200 7450 2500 7450
Wire Wire Line
	2500 7150 2500 7025
Connection ~ 2500 7025
Wire Wire Line
	2500 7025 2200 7025
Wire Wire Line
	2500 7350 2500 7450
Connection ~ 2500 7450
Wire Wire Line
	2500 7450 2775 7450
Wire Wire Line
	2775 7150 2775 7025
Connection ~ 2775 7025
Wire Wire Line
	2775 7025 2500 7025
Wire Wire Line
	2775 7350 2775 7450
Connection ~ 2775 7450
Wire Wire Line
	2775 7450 3050 7450
Wire Wire Line
	3050 7150 3050 7025
Connection ~ 3050 7025
Wire Wire Line
	3050 7025 2775 7025
Wire Wire Line
	3050 7350 3050 7450
Connection ~ 3050 7450
Wire Wire Line
	3050 7450 3325 7450
Wire Wire Line
	3325 7150 3325 7025
Connection ~ 3325 7025
Wire Wire Line
	3325 7025 3050 7025
Wire Wire Line
	3325 7350 3325 7450
Connection ~ 3325 7450
Wire Wire Line
	3325 7450 3600 7450
Wire Notes Line style solid
	6975 6525 475  6525
Text Notes 1905 6680 2    79   Italic 16
Decoupling capacitors
Wire Notes Line style solid
	6975 6525 6975 5250
Wire Notes Line style solid
	6975 5250 11225 5250
Text Notes 8405 5410 2    79   Italic 16
Decoupling capacitors
$Comp
L izirunf7-rescue:Crystal_GND2_Small-device-linyboard-rescue Y?
U 1 1 5C6420CA
P 1850 2150
AR Path="/5C061F84/5C6420CA" Ref="Y?"  Part="1" 
AR Path="/5C0272AF/5C6420CA" Ref="Y4"  Part="1" 
AR Path="/5C6420CA" Ref="Y4"  Part="1" 
F 0 "Y4" H 1795 1940 59  0000 L CIB
F 1 "25Mhz" H 1755 2260 39  0000 L CIB
F 2 "MyFootPrint:OSC_4P_2x1_6" H 1850 2150 39  0001 L TIB
F 3 "~" H 1850 2150 39  0001 L TIB
F 4 "XRCGB25M000F3G00R0" H 1850 1975 39  0001 L TIB "Manufacture Ref"
F 5 "https://www.mouser.fr/ProductDetail/Murata-Electronics/XRCGB25M000F3G00R0?qs=sGAEpiMZZMsBj6bBr9Q9aUR2LHkDqBwHstOVAfCKDLA%3d" H 1850 2150 39  0001 L TIB "Distributor"
	1    1850 2150
	1    0    0    -1  
$EndComp
NoConn ~ 1850 2275
$Comp
L device:C_Small C?
U 1 1 5C6420D2
P 2100 2425
AR Path="/5C061F84/5C6420D2" Ref="C?"  Part="1" 
AR Path="/5C0272AF/5C6420D2" Ref="C63"  Part="1" 
F 0 "C63" H 1900 2355 59  0000 L CIB
F 1 "6pF" H 1970 2500 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 2100 2425 39  0001 L TIB
F 3 "~" H 2100 2425 39  0001 L TIB
F 4 "https://fr.farnell.com/multicomp/mc0402n6r0d500ct/condensateur-6pf-50v-c0g-np0-0402/dp/1758936" H 0   0   39  0001 L TIB "Distributor"
F 5 "MC0402N6R0D500CT" H 0   0   39  0001 L TIB "Manufacture Ref"
	1    2100 2425
	-1   0    0    1   
$EndComp
$Comp
L device:C_Small C?
U 1 1 5C6420D9
P 1600 2425
AR Path="/5C061F84/5C6420D9" Ref="C?"  Part="1" 
AR Path="/5C0272AF/5C6420D9" Ref="C62"  Part="1" 
F 0 "C62" H 1635 2350 59  0000 L CIB
F 1 "6pF" H 1635 2505 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1600 2425 39  0001 L TIB
F 3 "~" H 1600 2425 39  0001 L TIB
F 4 "https://fr.farnell.com/multicomp/mc0402n6r0d500ct/condensateur-6pf-50v-c0g-np0-0402/dp/1758936" H 0   0   39  0001 L TIB "Distributor"
F 5 "MC0402N6R0D500CT" H 0   0   39  0001 L TIB "Manufacture Ref"
	1    1600 2425
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C6420E0
P 1850 2675
AR Path="/5C061F84/5C6420E0" Ref="#PWR?"  Part="1" 
AR Path="/5C0272AF/5C6420E0" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 1850 2425 59  0001 L CIB
F 1 "GND" H 1855 2502 39  0000 L CIB
F 2 "" H 1850 2675 39  0001 L TIB
F 3 "" H 1850 2675 39  0001 L TIB
	1    1850 2675
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2100 2525 2100 2625
Wire Wire Line
	2100 2625 1850 2625
Wire Wire Line
	1850 2625 1600 2625
Wire Wire Line
	1600 2625 1600 2525
Connection ~ 1850 2625
Wire Wire Line
	2100 2325 2100 2150
Wire Wire Line
	2100 2150 1950 2150
Wire Wire Line
	1750 2150 1600 2150
Wire Wire Line
	1600 2150 1600 2325
$Comp
L device:Crystal_Small Y3
U 1 1 5C65233E
P 1850 1350
F 0 "Y3" H 1795 1470 59  0000 L CIB
F 1 "32.786 Khz" H 1675 1230 39  0000 L CIB
F 2 "MyFootPrint:OSCI_3_2_X1_5" H 1850 1350 39  0001 L TIB
F 3 "~" H 1850 1350 39  0001 L TIB
	1    1850 1350
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C?
U 1 1 5C6527E4
P 2100 1075
AR Path="/5C061F84/5C6527E4" Ref="C?"  Part="1" 
AR Path="/5C0272AF/5C6527E4" Ref="C61"  Part="1" 
F 0 "C61" H 2125 1150 59  0000 L CIB
F 1 "6pF" H 2125 1000 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 2100 1075 39  0001 L TIB
F 3 "~" H 2100 1075 39  0001 L TIB
F 4 "https://fr.farnell.com/multicomp/mc0402n6r0d500ct/condensateur-6pf-50v-c0g-np0-0402/dp/1758936" H 0   0   39  0001 L TIB "Distributor"
F 5 "MC0402N6R0D500CT" H 0   0   39  0001 L TIB "Manufacture Ref"
	1    2100 1075
	1    0    0    -1  
$EndComp
$Comp
L device:C_Small C?
U 1 1 5C65286C
P 1600 1075
AR Path="/5C061F84/5C65286C" Ref="C?"  Part="1" 
AR Path="/5C0272AF/5C65286C" Ref="C60"  Part="1" 
F 0 "C60" H 1625 1150 59  0000 L CIB
F 1 "6pF" H 1625 1000 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1600 1075 39  0001 L TIB
F 3 "~" H 1600 1075 39  0001 L TIB
F 4 "https://fr.farnell.com/multicomp/mc0402n6r0d500ct/condensateur-6pf-50v-c0g-np0-0402/dp/1758936" H 0   0   39  0001 L TIB "Distributor"
F 5 "MC0402N6R0D500CT" H 0   0   39  0001 L TIB "Manufacture Ref"
	1    1600 1075
	1    0    0    -1  
$EndComp
Wire Wire Line
	2975 1525 2100 1525
Wire Wire Line
	2100 1525 2100 1350
Wire Wire Line
	1600 1625 1600 1350
Connection ~ 1600 1350
Wire Wire Line
	1600 1350 1600 1175
Wire Wire Line
	1600 1350 1750 1350
Wire Wire Line
	1950 1350 2100 1350
Connection ~ 2100 1350
Wire Wire Line
	2100 1350 2100 1175
$Comp
L power:GND #PWR?
U 1 1 5C6B7857
P 1850 825
AR Path="/5C061F84/5C6B7857" Ref="#PWR?"  Part="1" 
AR Path="/5C0272AF/5C6B7857" Ref="#PWR063"  Part="1" 
F 0 "#PWR063" H 1850 575 59  0001 L CIB
F 1 "GND" H 1785 680 39  0000 L CIB
F 2 "" H 1850 825 39  0001 L TIB
F 3 "" H 1850 825 39  0001 L TIB
	1    1850 825 
	-1   0    0    1   
$EndComp
Wire Wire Line
	1600 975  1600 875 
Wire Wire Line
	1600 875  1850 875 
Wire Wire Line
	2100 875  2100 975 
Wire Wire Line
	1850 825  1850 875 
Connection ~ 1850 875 
Wire Wire Line
	1850 875  2100 875 
Wire Wire Line
	1600 1625 2975 1625
Wire Wire Line
	1850 2675 1850 2625
Connection ~ 1600 2150
Connection ~ 2100 2150
Text HLabel 2625 2075 0    50   BiDi ~ 0
PB14
Text HLabel 2625 2175 0    50   BiDi ~ 0
PB7
Wire Wire Line
	2975 2075 2625 2075
Wire Wire Line
	2625 2175 2975 2175
Text HLabel 2625 2575 0    50   BiDi ~ 0
PB4
Text HLabel 2625 2675 0    50   BiDi ~ 0
PB3
Wire Wire Line
	2975 2575 2625 2575
Wire Wire Line
	2975 2675 2625 2675
Text HLabel 2625 2825 0    50   BiDi ~ 0
PD12
Text HLabel 2625 2925 0    50   BiDi ~ 0
PD13
Wire Wire Line
	2975 2825 2625 2825
Wire Wire Line
	2625 2925 2975 2925
Text HLabel 2625 2325 0    50   BiDi ~ 0
PC6
Wire Wire Line
	2625 2325 2975 2325
Text HLabel 2625 3025 0    50   BiDi ~ 0
PD11
Text HLabel 2625 3275 0    50   BiDi ~ 0
PA11
Text HLabel 2625 3175 0    50   BiDi ~ 0
PA12
Text HLabel 2625 2425 0    50   BiDi ~ 0
PG9
Wire Wire Line
	2975 3025 2625 3025
Wire Wire Line
	2975 3275 2625 3275
Wire Wire Line
	2625 3175 2975 3175
Wire Wire Line
	2625 2425 2975 2425
Text HLabel 2625 3525 0    50   Input ~ 0
POWER_KEY_OUT
Text HLabel 2625 3625 0    50   Output ~ 0
PWR_MODE
Text HLabel 2625 3825 0    50   Output ~ 0
BAT_HCHG
Text HLabel 2625 3925 0    50   Output ~ 0
BAT_SUSP
Text HLabel 2625 3425 0    50   Output ~ 0
PS_HOLD
Text HLabel 2625 4025 0    50   Input ~ 0
INT_POWER
Text HLabel 2630 5325 0    50   Output ~ 0
PA0_WKUP
Wire Wire Line
	2625 4025 2975 4025
Text HLabel 2570 5700 0    50   Input ~ 0
NRST
$Comp
L power:+3V3 #PWR075
U 1 1 5CCA53F2
P 1835 5295
F 0 "#PWR075" H 1835 5145 59  0001 L CIB
F 1 "+3V3" H 1750 5430 39  0000 L CIB
F 2 "" H 1835 5295 39  0001 L TIB
F 3 "" H 1835 5295 39  0001 L TIB
	1    1835 5295
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1835 5295 1835 5315
$Comp
L device:R_Small R52
U 1 1 5CD03EC6
P 5840 5225
F 0 "R52" H 5880 5230 59  0000 L CIB
F 1 "10k" V 5840 5175 39  0000 L CIB
F 2 "Resistor_SMD:R_0402_1005Metric" H 5840 5225 39  0001 L TIB
F 3 "~" H 5840 5225 39  0001 L TIB
	1    5840 5225
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR074
U 1 1 5CD03ED2
P 5840 4970
F 0 "#PWR074" H 5840 4820 59  0001 L CIB
F 1 "+3V3" H 5755 5135 39  0000 L CIB
F 2 "" H 5840 4970 39  0001 L TIB
F 3 "" H 5840 4970 39  0001 L TIB
	1    5840 4970
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR077
U 1 1 5CD03ED8
P 5840 5925
F 0 "#PWR077" H 5840 5675 59  0001 L CIB
F 1 "GND" H 5780 5745 39  0000 L CIB
F 2 "" H 5840 5925 39  0001 L TIB
F 3 "" H 5840 5925 39  0001 L TIB
	1    5840 5925
	1    0    0    -1  
$EndComp
Wire Wire Line
	5840 4970 5840 5125
Wire Wire Line
	5840 5850 5840 5925
Wire Wire Line
	5840 5325 5840 5425
Connection ~ 5840 5425
Wire Wire Line
	5840 5425 5840 5650
Text HLabel 5075 5325 2    50   BiDi ~ 0
PA13
Text HLabel 5075 5225 2    50   BiDi ~ 0
PA14
Wire Wire Line
	4875 5325 5075 5325
Wire Wire Line
	4875 5225 5075 5225
Text HLabel 5075 5075 2    50   BiDi ~ 0
PB9
Text HLabel 5075 4975 2    50   BiDi ~ 0
PB8
Text HLabel 5075 4875 2    50   BiDi ~ 0
PK4
Wire Wire Line
	4875 5075 5075 5075
Wire Wire Line
	4875 4975 5075 4975
Wire Wire Line
	4875 4875 5075 4875
Text HLabel 5075 4775 2    50   BiDi ~ 0
PK3
Text HLabel 5075 4675 2    50   BiDi ~ 0
PJ15
Text HLabel 5075 4575 2    50   BiDi ~ 0
PD6
Wire Wire Line
	4875 4775 5075 4775
Wire Wire Line
	4875 4675 5075 4675
Wire Wire Line
	4875 4575 5075 4575
Text HLabel 5075 4475 2    50   BiDi ~ 0
PG12
Text HLabel 5075 4375 2    50   BiDi ~ 0
PE4
Wire Wire Line
	4875 4475 5075 4475
Wire Wire Line
	4875 4375 5075 4375
Text HLabel 5075 4225 2    50   BiDi ~ 0
PD3
Text HLabel 5075 4125 2    50   BiDi ~ 0
PC7
Text HLabel 5075 4025 2    50   BiDi ~ 0
PH4
Text HLabel 5075 3925 2    50   BiDi ~ 0
PJ13
Text HLabel 5075 3825 2    50   BiDi ~ 0
PJ12
Text HLabel 5075 3725 2    50   BiDi ~ 0
PA6
Text HLabel 5075 3625 2    50   BiDi ~ 0
PE6
Text HLabel 5075 3525 2    50   BiDi ~ 0
PE5
Text HLabel 5075 3375 2    50   BiDi ~ 0
PG6
Text HLabel 5075 3275 2    50   BiDi ~ 0
PA8
Text HLabel 5075 3175 2    50   BiDi ~ 0
PA9
Text HLabel 5075 3075 2    50   BiDi ~ 0
PJ3
Text HLabel 5075 2975 2    50   BiDi ~ 0
PJ2
Text HLabel 5075 2875 2    50   BiDi ~ 0
PJ1
Text HLabel 5075 2775 2    50   BiDi ~ 0
PH3
Text HLabel 5075 2675 2    50   BiDi ~ 0
PI15
Text HLabel 5075 2525 2    50   BiDi ~ 0
PK7
Text HLabel 5075 2425 2    50   BiDi ~ 0
PI13
Text HLabel 5075 2325 2    50   BiDi ~ 0
PI12
Text HLabel 5075 2175 2    50   BiDi ~ 0
PA15
Text HLabel 5075 2025 2    50   BiDi ~ 0
DSI_CKN
Text HLabel 5075 1925 2    50   BiDi ~ 0
DSI_CKP
Text HLabel 5075 1825 2    50   BiDi ~ 0
DSI_D1N
Text HLabel 5075 1725 2    50   BiDi ~ 0
DSI_D1P
Text HLabel 5075 1625 2    50   BiDi ~ 0
DSI_D0N
Text HLabel 5075 1525 2    50   BiDi ~ 0
DSI_D0P
Wire Wire Line
	4875 4225 5075 4225
Wire Wire Line
	5075 4125 4875 4125
Wire Wire Line
	4875 4025 5075 4025
Wire Wire Line
	5075 3925 4875 3925
Wire Wire Line
	4875 3825 5075 3825
Wire Wire Line
	5075 3725 4875 3725
Wire Wire Line
	4875 3625 5075 3625
Wire Wire Line
	5075 3525 4875 3525
Wire Wire Line
	4875 3375 5075 3375
Wire Wire Line
	4875 2025 5075 2025
Wire Wire Line
	4875 1925 5075 1925
Wire Wire Line
	4875 1825 5075 1825
Wire Wire Line
	4875 1725 5075 1725
Wire Wire Line
	4875 1625 5075 1625
Wire Wire Line
	4875 1525 5075 1525
Wire Wire Line
	4875 2175 5075 2175
Wire Wire Line
	4875 2325 5075 2325
Wire Wire Line
	4875 2425 5075 2425
Wire Wire Line
	4875 2525 5075 2525
Wire Wire Line
	4875 2675 5075 2675
Wire Wire Line
	4875 2775 5075 2775
Wire Wire Line
	4875 2875 5075 2875
Wire Wire Line
	4875 2975 5075 2975
Wire Wire Line
	4875 3075 5075 3075
Wire Wire Line
	4875 3175 5075 3175
Wire Wire Line
	4875 3275 5075 3275
Wire Wire Line
	9425 3825 9600 3825
Wire Wire Line
	9425 3925 10100 3925
Wire Wire Line
	10300 3925 10775 3925
Wire Wire Line
	10775 3925 10775 3875
Wire Wire Line
	10775 3825 9800 3825
Wire Wire Line
	10775 3875 11050 3875
Wire Wire Line
	11050 3875 11050 3925
Connection ~ 10775 3875
Wire Wire Line
	10775 3875 10775 3825
Wire Notes Line style solid
	9400 3725 10600 3725
Wire Notes Line style solid
	10600 3725 10600 4575
Wire Notes Line style solid
	9400 3725 9400 4575
Wire Notes Line style solid
	9400 4575 10600 4575
NoConn ~ 2975 5025
NoConn ~ 2975 5125
Wire Wire Line
	2625 3525 2975 3525
Wire Wire Line
	2625 3425 2975 3425
Wire Wire Line
	2625 3625 2975 3625
NoConn ~ 2975 3725
Wire Wire Line
	2975 3825 2625 3825
Wire Wire Line
	2975 3925 2625 3925
NoConn ~ 2975 4225
NoConn ~ 2975 4325
NoConn ~ 2975 4425
NoConn ~ 2975 4625
NoConn ~ 2975 4725
Wire Wire Line
	2100 1875 2975 1875
Wire Wire Line
	2100 1875 2100 2150
Wire Wire Line
	2975 1775 1600 1775
Wire Wire Line
	1600 1775 1600 2150
Text Notes 5575 800  2    118  Italic 24
MCU
$Comp
L device:R_Small R?
U 1 1 5C86B996
P 2700 4825
AR Path="/5C204039/5C86B996" Ref="R?"  Part="1" 
AR Path="/5C0272AF/5C86B996" Ref="R50"  Part="1" 
F 0 "R50" V 2550 4775 59  0000 L CIB
F 1 "110k" V 2625 4750 39  0000 L CIB
F 2 "Resistor_SMD:R_0402_1005Metric" H 2700 4825 39  0001 L TIB
F 3 "~" H 2700 4825 39  0001 L TIB
	1    2700 4825
	0    1    1    0   
$EndComp
$Comp
L device:LED_Small D?
U 1 1 5C86B99E
P 2100 4825
AR Path="/5C204039/5C86B99E" Ref="D?"  Part="1" 
AR Path="/5C0272AF/5C86B99E" Ref="D6"  Part="1" 
F 0 "D6" H 2250 4875 59  0000 L CIB
F 1 "GREEN" H 2200 4950 39  0000 L CIB
F 2 "MyFootPrint:LED_2.0x1.25" V 2100 4825 39  0001 L TIB
F 3 "~" V 2100 4825 39  0001 L TIB
F 4 "https://fr.farnell.com/kingbright/kpt-2012cgck/led-vert-0805-cms/dp/2099235" H -250 0   39  0001 L TIB "Distributor"
F 5 "KPT-2012CGCK" H -250 0   39  0001 L TIB "Manufacture Ref"
	1    2100 4825
	1    0    0    -1  
$EndComp
NoConn ~ 2975 4525
Wire Wire Line
	2975 4825 2800 4825
$Comp
L device:R_Small R?
U 1 1 5CA2C7D4
P 2700 4925
AR Path="/5C204039/5CA2C7D4" Ref="R?"  Part="1" 
AR Path="/5C0272AF/5CA2C7D4" Ref="R51"  Part="1" 
F 0 "R51" V 2775 4875 59  0000 L CIB
F 1 "110k" V 2850 4825 39  0000 L CIB
F 2 "Resistor_SMD:R_0402_1005Metric" H 2700 4925 39  0001 L TIB
F 3 "~" H 2700 4925 39  0001 L TIB
	1    2700 4925
	0    1    1    0   
$EndComp
$Comp
L device:LED_Small D?
U 1 1 5CA2C7DA
P 2100 4925
AR Path="/5C204039/5CA2C7DA" Ref="D?"  Part="1" 
AR Path="/5C0272AF/5CA2C7DA" Ref="D7"  Part="1" 
F 0 "D7" H 2250 4875 59  0000 L CIB
F 1 "YELLOW" H 2225 4800 39  0000 L CIB
F 2 "MyFootPrint:LED_2.0x1.25" V 2100 4925 39  0001 L TIB
F 3 "~" V 2100 4925 39  0001 L TIB
F 4 "https://fr.farnell.com/kingbright/kpt-2012yc/led-jaune-0805-cms/dp/2099243" H -250 0   39  0001 L TIB "Distributor"
F 5 "KPT-2012YC" H -250 0   39  0001 L TIB "Manufacture Ref"
	1    2100 4925
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR073
U 1 1 5CA2C7E1
P 1225 4925
F 0 "#PWR073" H 1225 4675 59  0001 L CIB
F 1 "GND" H 1230 4752 39  0000 L CIB
F 2 "" H 1225 4925 39  0001 L TIB
F 3 "" H 1225 4925 39  0001 L TIB
	1    1225 4925
	1    0    0    -1  
$EndComp
Wire Wire Line
	2975 4925 2800 4925
Wire Wire Line
	1225 4925 1225 4825
Connection ~ 1225 4925
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 5C8A380E
P 10075 5700
F 0 "#FLG0104" H 10075 5775 59  0001 L CIB
F 1 "PWR_FLAG" H 10075 5874 39  0000 L CIB
F 2 "" H 10075 5700 39  0001 L TIB
F 3 "~" H 10075 5700 39  0001 L TIB
	1    10075 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9925 5700 10075 5700
Connection ~ 9925 5700
$Comp
L power:PWR_FLAG #FLG0109
U 1 1 5C8C5084
P 9325 5700
F 0 "#FLG0109" H 9325 5775 59  0001 L CIB
F 1 "PWR_FLAG" H 9325 5874 39  0000 L CIB
F 2 "" H 9325 5700 39  0001 L TIB
F 3 "~" H 9325 5700 39  0001 L TIB
	1    9325 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9175 5700 9325 5700
Connection ~ 9175 5700
Wire Wire Line
	9575 4075 9900 4075
$Comp
L power:PWR_FLAG #FLG0111
U 1 1 5CC80036
P 10050 4225
F 0 "#FLG0111" H 10050 4300 59  0001 L CIB
F 1 "PWR_FLAG" V 10050 4353 39  0000 L CIB
F 2 "" H 10050 4225 39  0001 L TIB
F 3 "~" H 10050 4225 39  0001 L TIB
	1    10050 4225
	0    1    1    0   
$EndComp
Wire Wire Line
	9900 4225 9900 4075
Connection ~ 9900 4075
Wire Wire Line
	9900 4075 10075 4075
Wire Wire Line
	9900 4225 10050 4225
$Comp
L Jumper:SolderJumper_2_Bridged JP?
U 1 1 5D256832
P 1725 4825
AR Path="/5C07B8C8/5D256832" Ref="JP?"  Part="1" 
AR Path="/5C0272AF/5D256832" Ref="JP4"  Part="1" 
F 0 "JP4" H 1725 4725 59  0000 L CIB
F 1 "Jumper" H 1725 4650 39  0001 L CIB
F 2 "Resistor_SMD:R_0402_1005Metric" H 1725 4825 39  0001 L TIB
F 3 "~" H 1725 4825 39  0001 L TIB
	1    1725 4825
	-1   0    0    1   
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP?
U 1 1 5D2BBC85
P 1725 4925
AR Path="/5C07B8C8/5D2BBC85" Ref="JP?"  Part="1" 
AR Path="/5C0272AF/5D2BBC85" Ref="JP5"  Part="1" 
F 0 "JP5" H 1725 5050 59  0000 L CIB
F 1 "Jumper" H 1725 5100 39  0001 L CIB
F 2 "Resistor_SMD:R_0402_1005Metric" H 1725 4925 39  0001 L TIB
F 3 "~" H 1725 4925 39  0001 L TIB
	1    1725 4925
	-1   0    0    1   
$EndComp
Wire Wire Line
	1875 4825 2000 4825
Wire Wire Line
	1875 4925 2000 4925
Wire Wire Line
	2200 4825 2600 4825
Wire Wire Line
	2200 4925 2600 4925
Wire Wire Line
	1225 4825 1575 4825
Wire Wire Line
	1225 4925 1575 4925
Text HLabel 2625 4125 0    50   Input ~ 0
~BAT_STAT_CHARG
Wire Wire Line
	2975 4125 2625 4125
$Comp
L conn:Test_Point TP16
U 1 1 5D66C2D7
P 9900 4300
F 0 "TP16" H 9950 4525 59  0000 L CIB
F 1 "Test_Point" H 9842 4417 39  0001 L CIB
F 2 "MyFootPrint:Test_Point_Pad_d0.5mm" H 10100 4300 39  0001 L TIB
F 3 "~" H 10100 4300 39  0001 L TIB
	1    9900 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	9900 4300 9900 4225
Connection ~ 9900 4225
$Comp
L device:L_Small L5
U 1 1 5C39D23C
P 7025 3775
F 0 "L5" V 7100 3730 59  0000 L CIB
F 1 "FCM1608KF-601T03" V 7180 3470 39  0000 L CIB
F 2 "Inductors_SMD:L_0805" H 7025 3775 39  0001 L TIB
F 3 "~" H 7025 3775 39  0001 L TIB
F 4 "https://fr.farnell.com/wurth-elektronik/742792042/perle-de-ferrite-0-5-ohm-0-2a/dp/1635716" H 0   0   39  0001 L TIB "Distributor"
F 5 "742792042" H 0   0   39  0001 L TIB "Manufacture Ref"
	1    7025 3775
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EF19A6D
P 995 5575
AR Path="/5EF19A6D" Ref="#PWR?"  Part="1" 
AR Path="/5C8F350B/5EF19A6D" Ref="#PWR?"  Part="1" 
AR Path="/5C90C3D3/5EF19A6D" Ref="#PWR?"  Part="1" 
AR Path="/5EB5BEF3/5EF19A6D" Ref="#PWR041"  Part="1" 
AR Path="/5C0272AF/5EF19A6D" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 995 5325 59  0001 C CIB
F 1 "GND" V 995 5405 39  0000 C CIB
F 2 "" H 995 5575 39  0001 C CIB
F 3 "" H 995 5575 39  0001 C CIB
	1    995  5575
	0    1    -1   0   
$EndComp
$Comp
L button_switch:B_Push BT1
U 1 1 5CC80DF5
P 1360 5575
F 0 "BT1" H 1360 5790 59  0000 C CIB
F 1 "BT RESET" H 1360 5505 39  0000 C CIB
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P" H 1360 5575 39  0001 C CIB
F 3 "" H 1360 5575 39  0001 C CIB
F 4 "B3U-1000P" H 1360 5420 39  0000 C CIB "FAB REF"
	1    1360 5575
	1    0    0    -1  
$EndComp
Wire Wire Line
	1835 5515 1835 5575
Connection ~ 1835 5575
Wire Wire Line
	1610 5575 1835 5575
Wire Wire Line
	2630 5325 2975 5325
Wire Wire Line
	2630 5425 2975 5425
Text HLabel 2630 5425 0    50   Output ~ 0
OTG_FAULT
Wire Wire Line
	1835 5575 2660 5575
Wire Wire Line
	2570 5700 2660 5700
Wire Wire Line
	2660 5700 2660 5575
Connection ~ 2660 5575
$Comp
L device:R_Small R53
U 1 1 5CCA4E8B
P 1835 5415
F 0 "R53" H 1610 5420 59  0000 L CIB
F 1 "10k" V 1835 5365 39  0000 L CIB
F 2 "Resistor_SMD:R_0402_1005Metric" H 1835 5415 39  0001 L TIB
F 3 "~" H 1835 5415 39  0001 L TIB
	1    1835 5415
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1110 5575 995  5575
$Comp
L device:C_Small C10
U 1 1 5E58C0A4
P 1835 5760
F 0 "C10" H 1860 5835 59  0000 L CIB
F 1 "100nF" H 1860 5685 39  0000 L CIB
F 2 "Capacitors_SMD:C_0402" H 1835 5760 39  0001 L TIB
F 3 "~" H 1835 5760 39  0001 L TIB
F 4 "MC0402B104K160CT" H -7740 1435 39  0001 L TIB "Manufacture Ref"
F 5 "https://fr.farnell.com/multicomp/mc0402b104k160ct/condensateur-0-1-f-16v-10-x7r/dp/1758896" H -7740 1435 39  0001 L TIB "Distributor"
	1    1835 5760
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E58C880
P 1835 5860
AR Path="/5C204039/5E58C880" Ref="#PWR?"  Part="1" 
AR Path="/5C0272AF/5E58C880" Ref="#PWR047"  Part="1" 
F 0 "#PWR047" H 1835 5610 59  0001 L CIB
F 1 "GND" H 1775 5705 39  0000 L CIB
F 2 "" H 1835 5860 39  0001 L TIB
F 3 "" H 1835 5860 39  0001 L TIB
	1    1835 5860
	1    0    0    -1  
$EndComp
Wire Wire Line
	1835 5660 1835 5575
Wire Wire Line
	2660 5575 2975 5575
$Comp
L device:R_Small R55
U 1 1 5CD03ECC
P 5840 5750
F 0 "R55" H 5880 5750 59  0000 L CIB
F 1 "NC" V 5840 5710 39  0000 L CIB
F 2 "Resistor_SMD:R_0402_1005Metric" H 5840 5750 39  0001 L TIB
F 3 "~" H 5840 5750 39  0001 L TIB
	1    5840 5750
	1    0    0    -1  
$EndComp
$Comp
L device:R_Small R19
U 1 1 5EBA85CC
P 5515 5750
F 0 "R19" H 5650 5755 59  0000 C CIB
F 1 "10K" V 5515 5750 39  0000 C CIB
F 2 "Resistor_SMD:R_0603_1608Metric" H 5515 5750 39  0001 C CIB
F 3 "~" H 5515 5750 39  0001 C CIB
	1    5515 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 5705 4900 5575
Connection ~ 4900 5575
Wire Wire Line
	4900 5575 4875 5575
Text HLabel 4945 5705 2    50   Input ~ 0
BOOT
Wire Wire Line
	4945 5705 4900 5705
Wire Wire Line
	4875 5425 5840 5425
$Comp
L power:GND #PWR?
U 1 1 5F573018
P 5515 5930
F 0 "#PWR?" H 5515 5680 59  0001 L CIB
F 1 "GND" H 5455 5750 39  0000 L CIB
F 2 "" H 5515 5930 39  0001 L TIB
F 3 "" H 5515 5930 39  0001 L TIB
	1    5515 5930
	1    0    0    -1  
$EndComp
Wire Wire Line
	5515 5850 5515 5930
Wire Wire Line
	5515 5575 5515 5650
Wire Wire Line
	4900 5575 5515 5575
$EndSCHEMATC
